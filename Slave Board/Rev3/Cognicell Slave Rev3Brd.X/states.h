/* 
 * File:   states.h
 * Author: User
 *
 * Created on April 6, 2014, 8:49 PM
 */

#ifndef STATES_H
#define	STATES_H

#ifdef	__cplusplus
extern "C" {
#endif

#define transPeriod 225

    void updateState() {
        if (prevState != currentState) {
            PEIE = 0;
        GIE = 0;
            resettingPWM = true;
            switch (currentState) {
                case POSITIVE:
                    setPositive(prevState);
                    break;
                case NEGATIVE:
                    setNegative(prevState);
                    break;
                case BYPASS:
                    setBypass(prevState);
                    break;
                case FLOAT:
                    setFloat(prevState);
                    break;
                default:
                    currentState = FLOAT;
                    break;
            }
            prevState = currentState;
            PEIE = 1;
        GIE = 1;
        }

    }

    void updatePWM() {
        if (previousPwm != pwmSetting) {            
            resettingPWM = true;
            if (((currentState == POSITIVE) || (currentState == NEGATIVE))) {
                switch (pwmSetting) {
                    case 0:
                        timeOn=0;
                        timeOff=0;
                         if (currentState == POSITIVE)
                            setPositive(BYPASS);

                    else if (currentState == NEGATIVE)
                        setNegative(BYPASS);
                        adjustPWM = false;
                        break;
                    case 1:
                        timeOn = 113;
                        timeOff = 362-timeOn;
                        adjustPWM = true;

                        break;
                    case 2:
                        timeOn = 213;
                        timeOff = 362-timeOn;
                        adjustPWM = true;
                        break;
                    case 3:
                        timeOn = 300;
                        timeOff =362-timeOn;//362-timeOn;
                        adjustPWM = true;
                        break;
                }
                continueCycle = false;
            } else {
                //adjustPWM = false;
                timeOn=0;
                timeOff=0;
            }
            previousPwm = pwmSetting;
        }       
    }


//Negative is
    // 1     0
    //
    // 0     1

void setNegative(enum State oldState) {
    switch (oldState) {
        case POSITIVE:
            R_UP = P_OFF;
            L_DN = N_OFF;
            _delay(transPeriod);
            L_UP = P_ON;
            R_DN = N_ON;
            break;
        case FLOAT:
            R_UP = P_OFF;
            L_DN = N_OFF;
            L_UP = P_ON;
            R_DN = N_ON;
            break;
        case BYPASS:
            L_DN = N_OFF;
            L_UP = P_ON;
            R_UP = P_OFF;
            _delay(transPeriod);
            R_DN = N_ON;
            break;
        default:
            R_UP = P_OFF;
            L_DN = N_OFF;
            L_UP = P_ON;
            _delay(transPeriod);
            R_DN = N_ON;
            break;
    }
}
//Positive is
    // 0     1
    //
    // 1     0
void setPositive(enum State oldState) {
    switch (oldState) {
        case NEGATIVE:
            L_UP = P_OFF;
            R_DN = N_OFF;
            _delay(transPeriod);
            R_UP = P_ON;
            L_DN = N_ON;
            break;
        case FLOAT:
            L_UP = P_OFF;
            R_DN = N_OFF;
            R_UP = P_ON;
            L_DN = N_ON;
            break;
        case BYPASS:
            L_UP = P_OFF;
            R_DN = N_OFF;
            R_UP = P_ON;
            _delay(transPeriod);
            L_DN = N_ON;
            break;
        default:
            L_UP = P_OFF;
            R_DN = N_OFF;
            _delay(transPeriod);
            R_UP = P_ON;
            L_DN = N_ON;
            break;
    }
}
//Bypass is
    // 1     1
    //
    // 0     0
void setBypass(enum State oldState) {
    switch (oldState) {
        //Negative is
    // 1     0
    //
    // 0     1
        case NEGATIVE:
            R_DN = N_OFF;
            _delay(transPeriod);
            R_UP = P_ON;
            //should already be done
            L_DN = N_OFF;
            L_UP = P_ON;
            break;

//Positive is
    // 0     1
    //
    // 1     0
        case POSITIVE:
            //turn off low
            L_DN = N_OFF;
            _delay(transPeriod);
            L_UP = P_ON;

            //should already be done
            R_DN = N_OFF;
            R_UP = P_ON;
            break;
        case FLOAT:
            L_UP = P_ON;
            R_UP = P_ON;
            //should already be done
            R_DN = N_OFF;
            L_DN = N_OFF;
            break;
        default:            
            L_DN = N_OFF;
            R_DN = N_OFF;
            _delay(transPeriod);
            L_UP = P_ON;
            R_UP = P_ON;
            break;
    }
}

void setFloat(enum State oldState) {
    switch (oldState) {
        case NEGATIVE:
            R_DN = N_OFF;
            L_UP = P_OFF;
            L_DN = N_OFF;
            R_UP = P_OFF;
            break;
        case POSITIVE:
            L_DN = N_OFF;
            R_UP = P_OFF;
            R_DN = N_OFF;
            L_UP = P_OFF;
            break;
        case BYPASS:
            L_UP = P_OFF;
            R_UP = P_OFF;
            R_DN = N_OFF;
            L_DN = N_OFF;
            break;
        default:
            L_UP = P_OFF;
            L_DN = N_OFF;
            R_UP = P_OFF;
            R_DN = N_OFF;
            break;
    }
}


#ifdef	__cplusplus
}
#endif

#endif	/* STATES_H */

