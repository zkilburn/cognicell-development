/* 
 * File:   UART.h
 * Author: User
 *
 * Created on April 6, 2014, 9:01 PM
 */

#ifndef UART_H
#define	UART_H


#define Fosc 32000000
#define Baud 19200
#define SBRG (Fosc/Baud/64)-1

#ifdef	__cplusplus
extern "C" {
#endif

    void initUART(){

         
         SPBRG = SBRG;//191
         BRGH = 0;
         BRG16 = 0;
         CREN = 1;
         SYNC = 0;
         SPEN = 1;

         BAUDCONbits.SCKP=1;
         TXEN = 1;

//         SPBRG = 12; // 38400 baud @ 4MHz = 12
//    TXSTA = 0x24; // setup USART transmit
//    CREN = 1;
//    SYNC = 0;
//    SPEN = 1;
    }
void putByteUSART(unsigned char data) {


        while (!TXIF);
        TXREG = data;
}

void putrs1USART(const char *data) {
    do {
        while (!TXIF);
        TXREG = *data;
    } while (*data++);
}

void putIntUSART(int value) {
    char lsb = (value >> 0) & 0xFF;
    char msb = (value >> 8) & 0xFF;
    putByteUSART(msb);
    putByteUSART(lsb);
}

void writeLEDDigits(int digitsNumber, int value) {
    char lsb = (value >> 0) & 0xFF;
    char msb = (value >> 8) & 0xFF;
    char checksum = 0;
    //send command set
    while (!TXIF);
    TXREG = (0x01);
    checksum = 0x01;
    //send the object type LED digits
    while (!TXIF);
    TXREG = (0x0F);
    checksum ^= 0x0F;
    //send which set of digits
    while (!TXIF);
    TXREG = (digitsNumber);
    checksum ^= digitsNumber;
    //send high byte
    while (!TXIF);
    TXREG = (msb);
    checksum ^= msb;
    //send low byte
    while (!TXIF);
    TXREG = (lsb);
    checksum ^= lsb;
    //send CRC
    while (!TXIF);
    TXREG = (checksum);
}

#ifdef	__cplusplus
}
#endif

#endif	/* UART_H */

