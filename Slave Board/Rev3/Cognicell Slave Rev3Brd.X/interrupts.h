/* 
 * File:   interrupts.h
 * Author: User
 *
 * Created on April 6, 2014, 8:45 PM
 */

#ifndef INTERRUPTS_H
#define	INTERRUPTS_H

#ifdef	__cplusplus
extern "C" {
#endif

    void interruptServiceInit() {
        PEIE = 1;
        GIE = 1;
    }

    void interrupt isr(void) {
        static int increment = 0, increment2 = 0;
        static bool speakKeeper = false;
        //If it was the timer0 interrupt hit
        if (INTF) {
            IOA = IO1, IOB = IO2;
            transmissionOver=false;
            checkInputs();
            INTF = 0;
        } else if (PIR1bits.TMR2IF) {
           // weAreAlive=true;
            PIR1bits.TMR2IF = 0;
        }else if (PIR1bits.TMR1IF) {


            if (spoke && speakKeeper) {
                speakKeeper = false;
                spoke = false;
            } else if (spoke) {
                speakKeeper = true;
            }
            if (increment2 > 5) {                
               weAreAlive = true;
                 
                requestADC();
                increment2 = 0;
            } else increment2++;

            //reset timer to determined value
            TMR1H = 0xFF;
            TMR1L = 0;

            PIR1bits.TMR1IF = 0;
        } else if (TMR0IF) {
           
            //debug setup
            if(!resettingPWM){
            if ((adjustPWM)&&((timeOn>0)||(timeOff>0))) {
                //if PWM is currently off
                if (!PWMON) {                    
                    //Set on time value
                    if (timeOn < 255) {
                        //set the timer value to full-timeOn
                        TMR0 = 255 - timeOn;
                        //if we are finishing a cycle
                        if(continueCycle){
                            timeOn+=255;
                            continueCycle = false;
                        }                        
                        else{ //first time through the loop
                            //turn on the PWM
                            if (currentState == POSITIVE)
                                setPositive(BYPASS);
                            else if(currentState==NEGATIVE)
                                setNegative(BYPASS);
                        }
                        //mark that PWM is on
                        PWMON = true;

                    } else {
                       //let cycle one timer interrupt
                        timeOn = timeOn - 255;
                        //flag to re add cycle time
                        continueCycle = true;
                        //turn on the PWM
                        if (currentState == POSITIVE)
                    setPositive(BYPASS);
                else if(currentState==NEGATIVE)
                    setNegative(BYPASS);
                    }
                }//if PWM is currently ON
                else {
                    //Set off time value
                    if (timeOff < 255) {
                        //timer value is full-timeOff
                        TMR0 = 255 - timeOff;
                        //if we are finishing a two step cycle
                        if (continueCycle) {
                            //refill the timer for next cycle
                            timeOff += 255;
                            //end cycle
                            continueCycle = false;
                        }//this is your first time through
                            else {
                                //turm off PWM                                
                                if (currentState == POSITIVE)
                                    setBypass(POSITIVE);
                                else if (currentState == NEGATIVE)
                                    setBypass(NEGATIVE);
                            }

                            //mark that PWM is off
                            PWMON = false;


                        } else {
                            //reduce timer by one cycle
                            timeOff -= 255;
                            //flag to re add time
                            continueCycle = true;
                            //turm off PWM
                            if (currentState == POSITIVE)
                                setBypass(POSITIVE);
                            else if (currentState == NEGATIVE)
                                setBypass(NEGATIVE);
                        }

                    }

                    //Set baseline timer value to 0x46 (74) for 100us period untouched
                    //TMR0 = 0x46;
                    //This makes the time between interrupts (181 counts) or 50us
                    //This makes the full period in timer pulses (255-74)*2 = 362

                } else {
                    continueCycle = false;
                    PWMON = true;
                    if (currentState == POSITIVE)
                        setPositive(BYPASS);
                    else if (currentState == NEGATIVE)
                        setNegative(BYPASS);
                    timeOn = 0;
                    timeOff = 0;
                }
            } else {

                resettingPWM = false;
            }
            TMR0IF = 0;

        } else if (ADIF) {
            if (ADCON0bits.CHS == 3) {
                voltageRead = readADC();
                voltageRead = ((voltageRead * (4400 / 512)) - 600);
                //            if(((unsigned char)voltageRead>100)&&((unsigned char)voltageRead<255)){
                //                volts=(unsigned char) voltageRead;
                //            }
                volts = (unsigned int) voltageRead;

                ADCON0bits.CHS = 29;
            } else {
                temperatureRead = (unsigned int) readADC();
                //temperatureRead=((0.659-((voltageRead/1000)(1-(temperatureRead/(255)))))/0.00132)-40;
                ADCON0bits.CHS = 3;
            }
            ADIF = 0;
        }  else {
        }

    }


#ifdef	__cplusplus
}
#endif

#endif	/* INTERRUPTS_H */

