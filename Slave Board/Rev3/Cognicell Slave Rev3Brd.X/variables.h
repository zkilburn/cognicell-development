/* 
 * File:   variables.h
 * Author: User
 *
 * Created on April 6, 2014, 8:42 PM
 */

#ifndef VARIABLES_H
#define	VARIABLES_H

#ifdef	__cplusplus
extern "C" {
#endif
    bool stateChanging=false;
    bool IOA , IOB;
    enum State currentState;
    enum State prevState;
    bool spoke = false;
    float voltageRead;
    volatile unsigned int temperatureRead;
    volatile unsigned int volts;
    float LEDdelay = 0;
    bool weAreAlive = false;
    static bool adjustPWM = false;
    static bool continueCycle = false;
    static unsigned int timeOn = 0, timeOff = 0;
    unsigned char pwmSetting;
    unsigned char previousPwm;
    bool resettingPWM;
    bool OUTPUT_ON=false;
    bool inputChangeFlagSet = false;
    bool transmissionOver=true;

#ifdef	__cplusplus
}
#endif

#endif	/* VARIABLES_H */

