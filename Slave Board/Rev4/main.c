/* 
 * File:   main.c
 * Author: Zac Kilburn
 *
 * Created on March 19, 2014
 *
 * CogniCell slave
 *
 *   Slave board has 4 basic states it can observe
 *      1. Float - CogniCell will not pass current
 *      2. Bypass - CogniCell will allow current to pass without affecting
 *                      the voltage of the system operation (terminals A-B)
 *      3. Positive - CogniCell orients positive voltage to terminals A-B
 *      4. Negative - CogniCell orients negative voltage to terminals A-B
 *
 *   System will change states by tracking a previous state and
 *      timing the transistion accordingly
 *
 *   Analog voltage measurement on VCELL pin (A4) will be recorded
 *          and when requested sent through IO_OUT pin (A1)
 *
 *   Commands will be received through ENABLE_IN(C3), IO1 (C4) and IO2 (C5)
 *
 *   Pin A5 is the enable pin of the onboard DC/DC 5v boost IC
 *
 *   -5/28/14
 *      -The inputs will be directed to a state change when the interrupt is
 *          set for the enable pin transistion.
 *      -This implies that every time the state is wanted to be changed
 *          the master sets the correct state on the pins and toggles the
 *          ENABLE pin to transition the slave.
 *      -the updateState() call allows this interrupt managed state to be
 *          caught by the main loop allowing the system to transistion
 *          between states according to the systems instruction
 *
 *      -need to verify state changes should occur in the main loop of code
 *          and not to be more embedded into the interrupt timing system
 *          to allow for finer control (currently should only take a few (hundred?)
 *              nanoseconds to notice the change inside interrupt *quote needs citation*
 *
 *      -ADC operations are guided by timer 0 interrupt, the ADC cycles
 *          independantly of the main loop/ master communications
 *
 *      -Timer0 sets the ADC go
 *      -ADC sets ADC done and resets the timer 0 module
 *
 *
 *
 *
 *
 */

// PIC16F1704 Configuration Bit Settings

// 'C' source line config statements
#define _XTAL_FREQ 32000000
#include <xc.h>




#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


#include "definitions.h"    //#defines
#include "variables.h"      //variables
bool timerSet=false;
bool PWMON=false;
#include "timers.h"

#include "methods.h"        //method declarations

//      -ADC.h carrys the methods to get FVR and ADC configured and
//           to carry out ADC reads raw
//           -conversion is done in interrupts.h
#include "ADC.h"            //ADC systems

//       -UART.h provides methods to manipulate the Tx lines specifically as
//           well as module initialization
#include "UART.h"

//       -interrups.h contains the initialization of timer modules, interrupt
//           pin as well as the actual interupt method itself
#include "interrupts.h"     //interrupt handler + init

//      -initialize.h contains systemInit() which can be used to change which
//          modules boot up.
#include "initialize.h"     //initialize the pins


//      -states.h contains methods to manipulate states of the cognicell
#include "states.h"         //all of the switching states logic and handling

//       -inputs.h manages the input check that occurs within the interrupt to
//          determine which state the master has told us to enter
#include "inputs.h"         //all of the input/output logic

int main(int argc, char** argv) {
    systemInit();
    


    
//    while (1){
//        currentState = BYPASS;
//        updateState();
//        __delay_ms(50);
//        WDTreset();
//
//        currentState = POSITIVE;
//        updateState();
//        __delay_ms(50);
//        WDTreset();
//
//        currentState = BYPASS;
//        updateState();
//        __delay_ms(50);
//        WDTreset();
//
//        currentState = NEGATIVE;
//        updateState();
//        __delay_ms(50);
//        WDTreset();
//
//    }
    while (1) {
        //updateState();
        //updatePWM();
        //LEDofLife();
        WDTreset();
        if (((!IO2) && (!IO1)) && (ENABLE_IN)) {
            if (!spoke) {
                putIntUSART((int) volts);
                putIntUSART((int) temperatureRead);
                spoke = true;
            }
        }
    }
    return (EXIT_SUCCESS);
}


