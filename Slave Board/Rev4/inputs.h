/* 
 * File:   inputs.h
 * Author: User
 *
 * Created on April 6, 2014, 8:49 PM
 */

#ifndef INPUTS_H
#define	INPUTS_H

#ifdef	__cplusplus
extern "C" {
#endif

    //Pleasee only get to this method if you intend to switch states

    void checkInputs() {
          
        if (!ENABLE_IN) {
            //first pulse says what direction to set
            if (!inputChangeFlagSet&&!transmissionOver) {  //

                switch (IOA) {
                    case 1:
                        if (!IOB) //input 01 -- NEGATIVE
                        {
                            currentState = NEGATIVE;
                            inputChangeFlagSet = true;
                            transmissionOver = false;
                        } else //input 00 -- POSITIVE
                        {
                            currentState = POSITIVE;
                            transmissionOver = false;
                            inputChangeFlagSet = true;
                        }
                        break;
                    case 0:
                        if (!IOB) { //input 11 -- FLOAT
                            currentState = FLOAT;
                            transmissionOver = true;
                            inputChangeFlagSet = false;
                            pwmSetting = 0;
                        } else { //input 10 -- BYPASS
                            currentState = BYPASS;
                            transmissionOver = true;
                            inputChangeFlagSet = false;
                            pwmSetting = 0;
                        }
                        break;
                }
            }
                //second pulse says what PWM to set output - only when the positive or negative set happens
            else {
                switch (IOA) {
                    case 1:
                        if (!IOB) {//input 01 -- PWM 1/4
                            pwmSetting = 1;
                        } else { //input 00 -- PWM OFF
                            pwmSetting = 0;
                            timeOn = 0;
                            timeOff = 0;
                        }
                        break;
                    case 0:
                        if (!IOB) { //input 11 -- PWM 3/4
                            pwmSetting = 3;
                        } else { //input 10 -- PWM 2/4
                            pwmSetting = 2;
                        }
                        break;
                }
                continueCycle=false;
                transmissionOver = true;
                inputChangeFlagSet = false;
            }


        }

    }



#ifdef	__cplusplus
}
#endif

#endif	/* INPUTS_H */

