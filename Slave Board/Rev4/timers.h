/* 
 * File:   timers.h
 * Author: User
 *
 * Created on July 9, 2014, 5:23 PM
 */

#ifndef TIMERS_H
#define	TIMERS_H

#ifdef	__cplusplus
extern "C" {
#endif

    void initializeTimer0() {
        OPTION_REGbits.nWPUEN = 0; //Pull-ups (0-on/1-off)
        OPTION_REGbits.INTEDG = 0; //interrupt falling (inverted logic)
        
        OPTION_REGbits.PSA = 0; //Prescaler Enable Bit
        OPTION_REGbits.PS = 0; // 1:2 (000) -> 1:256 (111)
    }

    void interruptTimer0() {
        OPTION_REGbits.TMR0CS = 0; //Set the Timer to internal clock
        TMR0IF = 0;
        TMR0IE = 1;
    }

    void initializeTimer1() {
        T1CONbits.T1CKPS = 3; //T1 prescaler == (3=[1:8] ; 2 = [1:4] ; 1 = [1,2]  ; 0 = [1,1])
        T1CONbits.TMR1CS = 3; //timer clock select FOSC
        TMR1H = 0xF0;         //Set initial timer Register
        TMR1L = 0;
    }

    void interruptTimer1() {
        PIR1bits.TMR1IF = 0;
        PIE1bits.TMR1IE = 1;
        T1CONbits.TMR1ON = 1;
    }

    void initializeTimer2() {
        PR2=0x2F;
        T2CONbits.T2CKPS = 0;   //T2 prescaler == (3=[1:8] ; 2 = [1:4] ; 1 = [1,2]  ; 0 = [1,1])
        T2CONbits.T2OUTPS = 0;  //T2 Postscaler  0000 = 1:1 Postscaler
    }

    void interruptTimer2() {
        PIR1bits.TMR2IF = 0;
        PIE1bits.TMR2IE = 1;
        //T2CONbits.TMR2ON = 1;
    }

    void initTimer4() {


    }


#ifdef	__cplusplus
}
#endif

#endif	/* TIMERS_H */

