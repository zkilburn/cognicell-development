/* 
 * File:   states.h
 * Author: User
 *
 * Created on April 6, 2014, 8:49 PM
 */

#ifndef STATES_H
#define	STATES_H

#ifdef	__cplusplus
extern "C" {
#endif

#define transPeriod 225

    void updateState() {



        if ((prevState != currentState) && (!stateChanging)) {
            resettingPWM = true;
            switch (currentState) {
                case POSITIVE:
                    startPositive(prevState);
                    LED_PIN=1;
                    break;
                case NEGATIVE:
                    LED_PIN=1;
                    startNegative(prevState);
                    break;
                case BYPASS:
                    LED_PIN=0;
                    startBypass(prevState);
                    break;
                case FLOAT:
                    LED_PIN=0;
                    setFloatS();
                    break;
                default:
                    currentState = FLOAT;
                    break;
            }
            prevState = currentState;
            stateChanging = true;
            //set the timer to get us back here to handle the second half of switching
            TMR2=0;
            PR2=0x2F;
            PIR1bits.TMR2IF = 0;
            T2CONbits.TMR2ON=1;
        } else if (prevState != currentState) {
            resettingPWM = true;
            setFloatS();
            stateChanging=false;
        } else if (stateChanging) {
            resettingPWM = true;
            if (delayComplete) {
                switch (currentState) {
                    case POSITIVE:
                        finishPositive();
                        break;
                    case NEGATIVE:
                        finishNegative();
                        break;
                    case BYPASS:
                        finishBypass();
                        break;
                    case FLOAT:
                        setFloatS();
                        break;
                    default:
                        currentState = FLOAT;
                        break;
                }
                delayComplete=false;
                stateChanging = false;
            }
        }




    }

    void updatePWM() {
        if (previousPwm != pwmSetting) {
            resettingPWM = true;
            if (((currentState == POSITIVE) || (currentState == NEGATIVE))) {
                switch (pwmSetting) {
                    case 0:
                        timeOn = 0;
                        timeOff = 0;
                        if (currentState == POSITIVE)
                            setPositive(BYPASS);

                        else if (currentState == NEGATIVE)
                            setNegative(BYPASS);
                        adjustPWM = false;
                        break;
                    case 1:
                        timeOn = 113;
                        timeOff = 362 - timeOn;
                        adjustPWM = true;
                        resettingPWM= false;
                        break;
                    case 2:
                        timeOn = 213;
                        timeOff = 362 - timeOn;
                        adjustPWM = true;

                        resettingPWM= false;
                        break;
                    case 3:
                        timeOn = 300;
                        timeOff = 362 - timeOn; //362-timeOn;
                        adjustPWM = true;

                        resettingPWM= false;
                        break;
                }
                continueCycle = false;
            } else {
                //adjustPWM = false;
                timeOn = 0;
                timeOff = 0;
            }
            previousPwm = pwmSetting;
        }
    }


    //Negative is
    // 1     0
    //
    // 0     1

    void startNegative(enum State oldState) {
        switch (oldState) {
            case POSITIVE:
                R_UP = P_OFF;
                L_DN = N_OFF;
                _delay(transPeriod);
                L_UP = P_ON;
                break;
            case FLOAT:
                R_UP = P_OFF;
                L_DN = N_OFF;
                L_UP = P_ON;
                break;
            case BYPASS:
                L_DN = N_OFF;
                L_UP = P_ON;
                R_UP = P_OFF;
                break;
            default:
                R_UP = P_OFF;
                L_DN = N_OFF;
                L_UP = P_ON;
                break;
        }
    }

    void finishNegative() {
        R_DN = N_ON;

    }

    void setNegative(enum State oldState) {
        switch (oldState) {
            case POSITIVE:
                R_UP = P_OFF;
                L_DN = N_OFF;
                _delay(transPeriod);
                L_UP = P_ON;
                R_DN = N_ON;
                break;
            case FLOAT:
                R_UP = P_OFF;
                L_DN = N_OFF;
                L_UP = P_ON;
                R_DN = N_ON;
                break;
            case BYPASS:
                L_DN = N_OFF;
                L_UP = P_ON;
                R_UP = P_OFF;
                _delay(transPeriod);
                R_DN = N_ON;
                break;
            default:
                R_UP = P_OFF;
                L_DN = N_OFF;
                L_UP = P_ON;
                _delay(transPeriod);
                R_DN = N_ON;
                break;
        }
    }
    //Positive is
    // 0     1
    //
    // 1     0

    void startPositive(enum State oldState) {
        switch (oldState) {
            case NEGATIVE:
                L_UP = P_OFF;
                R_DN = N_OFF;
                _delay(transPeriod);
                R_UP = P_ON;
                break;
            case FLOAT:
                L_UP = P_OFF;
                R_DN = N_OFF;
                R_UP = P_ON;
                break;
            case BYPASS:
                L_UP = P_OFF;
                R_DN = N_OFF;
                R_UP = P_ON;
                break;
            default:
                L_UP = P_OFF;
                R_DN = N_OFF;
                _delay(transPeriod);
                R_UP = P_ON;
                break;
        }
    }

    void finishPositive() {
        L_DN = N_ON;
    }

    void setPositive(enum State oldState) {
        switch (oldState) {
            case NEGATIVE:
                L_UP = P_OFF;
                R_DN = N_OFF;
                _delay(transPeriod);
                R_UP = P_ON;
                L_DN = N_ON;
                break;
            case FLOAT:
                L_UP = P_OFF;
                R_DN = N_OFF;
                R_UP = P_ON;
                L_DN = N_ON;
                break;
            case BYPASS:
                L_UP = P_OFF;
                R_DN = N_OFF;
                R_UP = P_ON;
                _delay(transPeriod);
                L_DN = N_ON;
                break;
            default:
                L_UP = P_OFF;
                R_DN = N_OFF;
                _delay(transPeriod);
                R_UP = P_ON;
                L_DN = N_ON;
                break;
        }
    }
    //Bypass is
    // 1     1
    //
    // 0     0

    void startBypass() {
        L_DN = N_OFF;
        R_DN = N_OFF;
    }

    void finishBypass() {
        L_UP = P_ON;
        R_UP = P_ON;
    }

    void setBypass(enum State oldState) {
        switch (oldState) {
                //Negative is
                // 1     0
                //
                // 0     1
            case NEGATIVE:
                R_DN = N_OFF;
                _delay(transPeriod);
                R_UP = P_ON;
                //should already be done
                L_DN = N_OFF;
                L_UP = P_ON;
                break;

                //Positive is
                // 0     1
                //
                // 1     0
            case POSITIVE:
                //turn off low
                L_DN = N_OFF;
                _delay(transPeriod);
                L_UP = P_ON;

                //should already be done
                R_DN = N_OFF;
                R_UP = P_ON;
                break;
            case FLOAT:
                L_UP = P_ON;
                R_UP = P_ON;
                //should already be done
                R_DN = N_OFF;
                L_DN = N_OFF;
                break;
            default:
                L_DN = N_OFF;
                R_DN = N_OFF;
                _delay(transPeriod);
                L_UP = P_ON;
                R_UP = P_ON;
                break;
        }
    }

    void setFloatS() {
        R_DN = N_OFF;
        L_UP = P_OFF;
        L_DN = N_OFF;
        R_UP = P_OFF;
    }

    void setFloat(enum State oldState) {
        switch (oldState) {
            case NEGATIVE:
                R_DN = N_OFF;
                L_UP = P_OFF;
                L_DN = N_OFF;
                R_UP = P_OFF;
                break;
            case POSITIVE:
                L_DN = N_OFF;
                R_UP = P_OFF;
                R_DN = N_OFF;
                L_UP = P_OFF;
                break;
            case BYPASS:
                L_UP = P_OFF;
                R_UP = P_OFF;
                R_DN = N_OFF;
                L_DN = N_OFF;
                break;
            default:
                L_UP = P_OFF;
                L_DN = N_OFF;
                R_UP = P_OFF;
                R_DN = N_OFF;
                break;
        }
    }


#ifdef	__cplusplus
}
#endif

#endif	/* STATES_H */

