/* 
 * File:   definitions.h
 * Author: User
 *
 * Created on April 6, 2014, 8:41 PM
 */

#ifndef REV2DEFINITIONS_H
#define	REV2DEFINITIONS_H

#ifdef	__cplusplus
extern "C" {
#endif


#define WDTreset() asm("clrwdt")
//----------------Pin Definitions---------------
//Left side of bridge
#define L_UP LATCbits.LATC0
#define L_DN LATAbits.LATA2
//Right side of bridge
#define R_UP LATCbits.LATC1
#define R_DN LATCbits.LATC2

//Three Inputs from the master
#define ENABLE_IN PORTCbits.RC3
#define IO1 PORTCbits.RC4
#define IO2 PORTCbits.RC5


//PULL ups
#define ENABLE_PULL WPUCbits.WPUC3
#define IO1_PULL WPUCbits.WPUC4
#define IO2_PULL WPUCbits.WPUC5
#define IO_OUT_PULL WPUAbits.WPUA1
//Slave output to the master
#define IO_OUT LATAbits.LATA1
//Analog read voltage
#define VCELL LATAbits.LATA4
//boost enable
#define ENABLE_BOOST LATAbits.LATA5
#define LED_PIN LATAbits.LATA0
//Define MOSFET states by channel
#define P_ON 0
#define P_OFF 1
#define N_ON 1
#define N_OFF 0

#define WDT WDTCONbits.SWDTEN

enum State {
    POSITIVE = 0, NEGATIVE = 1, BYPASS = 2, FLOAT = 3, MAXENUMS = 4
};
 void interruptPin() {
        PPSLOCKED = 0;
        RA1PPS = 20; //UART configuration A1
        INTPPS = 0x13;
        PPSLOCKED = 1;
        INTF = 0;
        INTE = 1;
    }
#ifdef	__cplusplus
}
#endif

#endif	/* REV2DEFINITIONS_H */

