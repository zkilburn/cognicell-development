/* 
 * File:   variables.h
 * Author: User
 *
 * Created on April 6, 2014, 8:42 PM
 */

#ifndef VARIABLES_H
#define	VARIABLES_H

#ifdef	__cplusplus
extern "C" {
#endif
    volatile bool delayComplete=false;
    volatile bool stateChanging=false;
    volatile bool IOA , IOB;
    volatile enum State currentState;
    volatile enum State prevState;
    bool spoke = false;
    float voltageRead;
    volatile unsigned int temperatureRead;
    volatile unsigned int volts;
    float LEDdelay = 0;
    volatile bool weAreAlive = false;
    volatile static bool adjustPWM = false;
    volatile static bool continueCycle = false;
    volatile static unsigned int timeOn = 0, timeOff = 0;
    volatile unsigned char pwmSetting;
    volatile unsigned char previousPwm;
    volatile bool resettingPWM;
    bool OUTPUT_ON=false;
    volatile bool inputChangeFlagSet = false;
    volatile bool transmissionOver=true;

#ifdef	__cplusplus
}
#endif

#endif	/* VARIABLES_H */

