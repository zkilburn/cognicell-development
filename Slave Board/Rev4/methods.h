/* 
 * File:   methods.h
 * Author: User
 *
 * Created on April 6, 2014, 8:41 PM
 */

#ifndef METHODS_H
#define	METHODS_H

#ifdef	__cplusplus
extern "C" {
#endif
    void updatePWM();
    void systemInit();
    //Methods placeholders
    void initPins();
    void initializeTimer0();
    void interruptTimer0();
    void interruptServiceInit();
    void interruptPin();
    void initUART();
    void initADC();
    void initFVR(char gain);
    void disableADC();
    void initializeTimer2();
    void interruptTimer2();
    void putrs1USART(const char *data);
    void putByteUSART(unsigned char data);
    void putIntUSART(int value);

    void putByteUSART(unsigned char data);
    void putrs1USART(const char *data);

    void writeLEDDigits(int digitsNumber, int value);

    void updateState();
    void run_Simple_IO_OUT_TEST();
    void interrupt isr(void);
    void checkInputs();
    void requestADC();
    float readADC();

    void setPositive(enum State oldState);
    void setNegative(enum State oldState);
    void setBypass(enum State oldState);
    void setFloat(enum State oldState);

    void startPositive(enum State oldState);
    void startNegative(enum State oldState);
    void startBypass();
    void setFloatS();

    void finishPositive();
    void finishNegative();
    void finishBypass();
    
    void delay_ms(int time){
         for(int i=0;i<time;i++)
             _delay(32000);
    }

    void delay_us(int time){
        for(int i=0;i<time;i++)
            _delay(32);
    }

    void LEDofLife(){
        if(weAreAlive){
           LED_PIN=!LED_PIN;
            weAreAlive=false;
            WDTreset();
        }
        
    }


#ifdef DEBUG_IO
    bit state = 0;

    void run_Simple_IO_OUT_TEST() {

        while (1) {
            state = !state;
            IO_OUT = state;
            _delay(250);
        }
    }
#endif

#ifdef	__cplusplus
}
#endif

#endif	/* METHODS_H */

