/* 
 * File:   initialize.h
 * Author: User
 *
 * Created on April 6, 2014, 8:46 PM
 */
#include <xc.h>
#ifndef INITIALIZE_H
#define	INITIALIZE_H

#ifdef	__cplusplus
extern "C" {
#endif

    void initTempReading() {
        FVRCONbits.TSRNG = 1; //high range =1 ; low range=0
        FVRCONbits.TSEN = 1; //temp module enable
    }

    void initPins() {

        TRISA = 0X30; //Set pin I/O direction Port A
        ANSELA = 0x10;
        TRISC = 0x38; //Set pin I/O direction Port C
        ANSELC = 0;
        L_DN = 0;
        L_UP = 1;
        R_UP = 1;
        R_DN = 0;
        //ENABLE_BOOST = 1;
        LED_PIN = 1;
        _delay(5000);
        OPTION_REGbits.nWPUEN = 0;
        _delay(500);
        ENABLE_PULL = 1;
        IO1_PULL = 1; //pull ups
        IO2_PULL = 1; //pull ups
       
        prevState = BYPASS; //RECORD PREVIOUS STATE
        currentState = BYPASS; //RECORD CURRENT STATE
        L_UP = 0;
        R_UP = 0;
    }

    void initClk() {
        OSCCONbits.SCS = 0;
        OSCCONbits.IRCF = 0xE; //set osc freq to 8/32Mhz
        OSCCONbits.SPLLEN = 1; //Enable PLL if not configuration words
    }

    void initWatchdog() {
        WDTCONbits.WDTPS = 0x0F; //watchdog timer (A=1sec   B=2sec  C=4sec  D=8sec  F=16sec)
    }

    void systemInit() {
        //Clock configuration
        initClk();
        //pin configuration
        initPins();
        __delay_ms(1000);
        //watchdog enabled and config
        initWatchdog();
        //Fixed voltage reference init
        initFVR(2);
        //ADC init
        initADC();
        //Temperature Reading
        initTempReading();
        //UART init
        initUART();
        //Timer0 allows for PWM control
        initializeTimer0();
        interruptTimer0();
        //Timer 1 allows for LED blink of life and automation of the ADC read system
        initializeTimer1();
        interruptTimer1();
        //Timer 2 allows for state change timing
        initializeTimer2();
        interruptTimer2();
        //Interrupt pin allows for automated input handling
        interruptPin();
        //Overhead interrupt service init
        interruptServiceInit();

        WDTreset();

    }

 

#ifdef	__cplusplus
}
#endif

#endif	/* INITIALIZE_H */

