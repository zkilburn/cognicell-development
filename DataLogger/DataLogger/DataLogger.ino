#include <Timers.h>

#define cogni A0
#define solar A5
#define currentP A1
#define solarMonitor A4


Timers dataTimer(250);
Timers commTimer(1000);
uint16_t seconds;
float voltsSolar, voltsCogni, current,solarLevel;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(solarMonitor,INPUT);
  pinMode(cogni, INPUT);
  pinMode(solar, INPUT);
  pinMode(currentP, INPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
  dataTimer.updateTimer();
  if (dataTimer.timerDone()) {
    voltsSolar = analogRead(solar) * 0.035;
    voltsCogni = analogRead(cogni) * 0.040;
    current = ((((analogRead(currentP) *0.00488)-2.5)*33300)-7);
    solarLevel= analogRead(solarMonitor);  
}
  commTimer.updateTimer();
  if (commTimer.timerDone()) {
 
    
    Serial.print(seconds);
    Serial.print(",");
    Serial.print(voltsCogni);
    Serial.print(",");
    Serial.print(voltsSolar);
    Serial.print(",");
    Serial.print(current/1000);
    Serial.print(",");
    Serial.println((solarLevel));
    seconds++;

  }



}
