//every10.h
void everyTenMil() {
  if (tenMil.timerDone()) {
    
    updateCommunications();    
    
    if (stateChange) {
      cycle=0;
      cycle1=0;
      delayBetweenCells = 500;
      cellsCompleteCharging = 0;
      for (int i = 0; i < 10; i++) {
        batterySet[i]._pwmSetting = 4;
        cellChargingComplete[i] = false;
      }
      screenData.completeCharging=false;
      screenData.voltageSetOverride=false;
      screenData.voltageSetControl=0;
      
      switch (demoState)
      {
        case CYCLEDC:
          updateBypass();
          break;
        case POSITIVE:

          updateBypass();
          break;
        case NEGATIVE:

          updateBypass();
          break;
        case CYCLEAC:
           
          updateBypass();
          AC.setInterval(304);
          break;
        case NONE:
          updateBypass();

          break;
        case ALLPOSITIVE:
          updatePositive();
          break;
        case ALLNEGATIVE:
          updateNegative();
          break;
        case ALLBYPASS:
          updateBypass();
          break;
        case ALLFLOAT:
          updateFloat();
          break;
        case CHARGINGDC:
          vSet = inputData.voltageSet;
          updateVoltage(vSet);
          break;
        case PWMALL:
          updateBypass();
          break;
        case setPWM:
          updateBypass();
          break;
        case CONTROLSET:
          updateBypass();
          break;
        case SOLARCHARGE:
          dataGather();
          dV=0;
          dP=0;
          dI=0;
          updateVoltage(3);
          dataGather();
          startSolarCharge = true;
          break;
          
      }
      stateChange = false;
    }




   

    tenMil.resetTimer();
  }
}








