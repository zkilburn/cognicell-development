//Variables.h
//communications
EasyTransferCRC fromScreen, toScreen;
bool adjustedLevel=false;
bool cellChargingComplete[totalCellsAvailable];
//battery state management
uint16_t targetPositive, targetNegative, targetBypass, targetFloat;
uint16_t negativeBatts, positiveBatts, bypassBatts, floatBatts;
uint16_t activeCellLow, activeCellHigh;
uint8_t inactiveHigh, inactiveLow, chargingCellHigh;
uint8_t battCondition[totalCellsAvailable];
Cognicell batterySet[totalCellsAvailable];
uint16_t volts[totalCellsAvailable];
uint16_t temp[totalCellsAvailable];
uint8_t pwmSet[totalCellsAvailable];
//globals
uint8_t cellsCompleteCharging;
static demonstrationState demoState;
Timers  tenMil(150), fiftyMil(50), hundredMil(100), twoFiftyMil(250), fiveHundoMil(500), second(1000), twoSeconds(5000), fiveSeconds(5000);
TimersMicros AC(30);
int cycle = 0;
uint8_t cycle1=0;
bool liveOutput = false;
uint8_t numCellsActive = 0;
uint8_t numCellsPresent = 0;
uint8_t cellPresent[totalCellsAvailable];
int8_t vSet = 0;
bool stateChange = false;
int delayBetweenCells = 500; //800 //500000
bool chargingContactor = false;
bool motorContactor = false;
int powerLevel = 0;
int controlSet;
bool controlSetReached = true;

float voltsSolar, voltsCogni, current, solarLevel, solarPowerOutput;
float dV, dP, dI;
bool startSolarCharge = false;
unsigned int chargeSet = 3;
