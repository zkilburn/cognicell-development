//everySecond.h
void everySecond() {
  if (second.timerDone()) {

    if (demoState != CYCLEAC) {
      if (demoState == PWMALL) {
        static uint8_t getbigger = 1;
        static uint8_t getsmaller = 3;
        static bool goingup = true;
        static uint8_t ramp;

        if (!goingup) {
          if (getsmaller > 0) {

            batterySet[ramp].updateState(POS, getsmaller);
            getsmaller--;
          }
          else {

            batterySet[ramp].updateState(BYPASS);

            getsmaller = 3;
            ramp++;
            if (ramp > numCells) {
              goingup = true;
              ramp = 0;
            }
          }
        }
        else
        {
          if (getbigger == 4) {
            getbigger = 0;
            goingup = false;
          }

          batterySet[ramp].updateState(POS, getbigger);

          getbigger++;
          if (!goingup) {
            ramp++;
            if (ramp <= numCells) {
              goingup = true;
            }
            else {
              ramp = 0;
            }
          }
        }
      }
      else if (demoState == CYCLEDC) {
        delayBetweenCells = 1000;
        switch (cycle) {
        case 0:
          updatePositive();
          cycle++;
          break;
        case 1:
          updateBypass();
          cycle++;
          break;
        case 2:
          updateNegative();
          cycle++;
          break;
        case 3:
          updateBypass();
          cycle = 0;
          break;
        default:
          cycle = 0;
          break;
        }
      }
      updateVoltages();     
      debugBatteryInfo();
    }


    second.resetTimer();
  }
}




