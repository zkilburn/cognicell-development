int rollingUpdate = 0;
void writeToScreen() {
  switch (activeForm) {
    case 0:
      switch (rollingUpdate) {
        case 0:
          writeObject(LEDDIGITS, lowVoltageDigits, (batterySet[0].returnVoltage() / 100));
          break;
        case 1:
          writeObject(LEDDIGITS, recommendedMaxDigits, numCells * 3);
          break;
        case 2:
          writeObject(LEDDIGITS, cellCountDigits, numCells);
          break;
        case 3:
          writeObject(LEDDIGITS, maxVoltageDigits, (int8_t) (sumVoltage() / 100));
          break;
        case 4:
          writeObject(GUAGE, 0, constrain((char)((  ((sumVoltage() / 100) - (2.8 * numCells))  /  ((3.7 * numCells) - (2.8 * numCells))  ) * 100),0,100));
          break;

        case 5:
          writeObject(USERLED, 1, liveOutput);
          break;
      }
      rollingUpdate = rollingUpdate + 1;
      if (rollingUpdate >= 6) {
        rollingUpdate = 0;
      }
      break;

    case 1:
      switch (rollingUpdate) {
        case 0:

          writeObject(LEDDIGITS, 0, batterySet[0].returnVoltage());
          break;
        case 1:
          writeObject(LEDDIGITS, 1, batterySet[1].returnVoltage());
          break;
        case 3:
          writeObject(LEDDIGITS, 2, batterySet[2].returnVoltage());
          break;
        case 4:
          writeObject(LEDDIGITS, 3, batterySet[3].returnVoltage());
          break;
        case 5:
          writeObject(LEDDIGITS, 4, batterySet[4].returnVoltage());
          break;
        case 6:
          writeObject(LEDDIGITS, 5, batterySet[5].returnVoltage());
          break;
        case 7:
          writeObject(LEDDIGITS, 12, batterySet[0].returnTemp());
          break;
        case 8:
          writeObject(LEDDIGITS, 13, batterySet[1].returnTemp());
          break;
        case 9:
          writeObject(LEDDIGITS, 14, batterySet[2].returnTemp());
          break;
        case 10:
          writeObject(LEDDIGITS, 15, batterySet[3].returnTemp());
          break;
        case 11:
          writeObject(LEDDIGITS, 16, batterySet[4].returnTemp());
          break;
        case 12:
          writeObject(LEDDIGITS, 17, batterySet[5].returnTemp());
          break;
        case 13:
          writeObject(USERLED, 5, liveOutput);
          break;
        case 14:
          writeObject(LEDDIGITS, 6, sumVoltage());
          break;
      }
      rollingUpdate = rollingUpdate + 1;
      if (rollingUpdate >= 15) {
        rollingUpdate = 0;
      }
      break;

    case 2:
      switch (rollingUpdate) {
        case 0:
          writeObject(LEDDIGITS, voltageSetpointDigits, abs(vSet));
          break;
        case 1:
          if (!liveOutput)
            writeObject(ANGULARMETER, 0, vSet + 36);
          else
            writeObject(ANGULARMETER, 0, vSet + 36);
          break;
        case 2:
          writeObject(USERLED, outputActiveLED, liveOutput);
          break;
        case 3:
          writeObject(LEDDIGITS, 58, abs(screenData.current / 100));
          break;
      }

      rollingUpdate = rollingUpdate + 1;
      if (rollingUpdate >= 4) {
        rollingUpdate = 0;
      }
      break;
    case 3:
      writeObject(USERLED, 4, liveOutput);
      break;
    case 4:

      //Output voltage
      int outputVoltage;
      switch (rollingUpdate) {
        case 0:
          if ((vSet != 0) && (liveOutput)) {
            outputVoltage = abs(vSet);
          }
          else {
            outputVoltage = 0;
          }
          writeObject(LEDDIGITS, 18, outputVoltage * 10);
          break;
        case 1:
          if (vSet > 0) {
            //Positive set point
            writeObject(LEDDIGITS, 19, (int)(vSet));
            //Negative set point
            writeObject(LEDDIGITS, 20, 0);
          }
          else if (vSet < 0) {
            //Positive set point
            writeObject(LEDDIGITS, 19, 0);
            //Negative set point
            writeObject(LEDDIGITS, 20, (int) (abs(vSet)));
          }
          else if (vSet == 0) {
            //Positive set point
            writeObject(LEDDIGITS, 19, 0);
            //Negative set point
            writeObject(LEDDIGITS, 20, 0);
            writeObject(SLIDER, 0, 0);
            writeObject(SLIDER, 1, 0);
          }

          break;
        case 2:
          writeObject(USERLED, 3, liveOutput);

          break;


      }

      rollingUpdate = rollingUpdate + 1;
      if (rollingUpdate >= 3) {
        rollingUpdate = 0;
      }
      break;

    case 5:
      //Charging control
      switch (rollingUpdate) {
        case 0:
          writeObject(LEDDIGITS, 21, chargingV);
          break;
        case 1:
          writeObject(LEDDIGITS, 22, screenData.cognicellVoltage);
          break;
        case 2:
          writeObject(LEDDIGITS, 23, abs(screenData.cognicellVoltage * (screenData.current / 100)));
          break;
        case 3:
          writeObject(GUAGE, 1, constrain((char)((  ((sumVoltage() / 100) - (2.8 * numCells))  /  ((3.7 * numCells) - (2.8 * numCells))  ) * 100),0,100));
          break;
        case 4:
          writeObject(LEDDIGITS, 54 , abs(screenData.current) / 100);
          break;
        case 5:
          writeObject(LEDDIGITS, 56, screenData.solarLevel);
          break;
        case 6:
          writeObject(LEDDIGITS, 55, screenData.solarVoltage * 10);
          break;
        case 7:
          writeObject(LEDDIGITS, 57, screenData.cellsComplete);
          break;
        case 8:
          writeObject(USERLED, 2, liveOutput);
          break;

      }
      rollingUpdate = rollingUpdate + 1;
      if (rollingUpdate >= 9) {
        rollingUpdate = 0;
      }
      break;
    case 6:
      switch (rollingUpdate) {
        case 0:
          writeObject(LEDDIGITS, 24, pwmSet[rollingUpdate]);
          break;
        case 1:
          writeObject(LEDDIGITS, 27, pwmSet[rollingUpdate]);
          break;
        case 2:
          writeObject(LEDDIGITS, 28, pwmSet[rollingUpdate]);
          break;
        case 3:
          writeObject(LEDDIGITS, 29, pwmSet[rollingUpdate]);
          break;
        case 4:
          writeObject(LEDDIGITS, 30, pwmSet[rollingUpdate]);
          break;
        case 5:
          writeObject(LEDDIGITS, 31, pwmSet[rollingUpdate]);
          break;
        case 6:
          writeObject(LEDDIGITS, 37, pwmSet[rollingUpdate]);
          break;
        case 7:
          writeObject(LEDDIGITS, 38, pwmSet[rollingUpdate]);
          break;
        case 8:
          writeObject(LEDDIGITS, 39, pwmSet[rollingUpdate]);
          break;
        case 9:
          writeObject(LEDDIGITS, 40, pwmSet[rollingUpdate]);
          break;
        case 10:
          writeObject(LEDDIGITS, 25, pwmSet[0]);
          break;
        case 11:
          writeObject(LEDDIGITS, 32, pwmSet[1]);
          break;
        case 12:
          writeObject(LEDDIGITS, 33, pwmSet[2]);
          break;
        case 13:
          writeObject(LEDDIGITS, 34, pwmSet[3]);
          break;
        case 14:
          writeObject(LEDDIGITS, 35, pwmSet[4]);
          break;
        case 15:
          writeObject(LEDDIGITS, 36, pwmSet[5]);
          break;
        case 16:
          writeObject(LEDDIGITS, 41, pwmSet[6]);
          break;
        case 17:
          writeObject(LEDDIGITS, 42, pwmSet[7]);
          break;
        case 18:
          writeObject(LEDDIGITS, 43, pwmSet[8]);
          break;
        case 19:
          writeObject(LEDDIGITS, 44, pwmSet[9]);
          break;
        case 20:
          writeObject(LEDDIGITS, 26, (uint16_t) (sumPWMVoltage() * 10));
          break;
      }
      rollingUpdate = rollingUpdate + 1;
      if (rollingUpdate >= 21) {
        rollingUpdate = 0;
      }
      break;
    case 8:
      writeObject(USERLED, 6, liveOutput);
      if (controlSet == 0) {
        writeObject(SLIDER, 2, 100);
      }
      break;
    case 9:
      switch (rollingUpdate) {
        case 0:
          writeObject(LEDDIGITS, 45, batterySet[6].returnVoltage());
          break;
        case 1:
          writeObject(LEDDIGITS, 49, batterySet[6].returnTemp());
          break;
        case 2:
          //Serial.print(rollingUpdate);
          writeObject(USERLED, 7, liveOutput);
          writeObject(LEDDIGITS, 46, batterySet[7].returnVoltage());
          writeObject(LEDDIGITS, 50, batterySet[7].returnTemp());
          break;
        case 3:
          writeObject(LEDDIGITS, 53, sumVoltage());
          //Serial.print(rollingUpdate);
 writeObject(LEDDIGITS,51,batterySet[8].returnTemp());
 writeObject(LEDDIGITS,52,batterySet[9].returnTemp());
          break;
        case 4:
          //Serial.print(rollingUpdate);
    writeObject(LEDDIGITS,47,batterySet[8].returnVoltage());
    writeObject(LEDDIGITS,48,batterySet[9].returnVoltage());
          break;
          case 5:          
          break;
          case 6:
          break;
      }
      //writeObject(LEDDIGITS,51,rollingUpdate);
      rollingUpdate = rollingUpdate + 1;
      if (rollingUpdate >= 4) {
        rollingUpdate = 0;
      }
      break;

    case 10:
      if ((rollingUpdate >= 0) && (rollingUpdate < 10)) {
        writeObject(USERLED, (rollingUpdate + 8), ((screenData.batterySet[rollingUpdate]._currentState == 0) && screenData.batterySet[rollingUpdate].returnActive()));
      }
      else if ((rollingUpdate >= 10) && (rollingUpdate < 20)) {
        writeObject(USERLED, (rollingUpdate + 8), ((screenData.batterySet[rollingUpdate - 10]._currentState == 1) && screenData.batterySet[rollingUpdate - 10].returnActive()));
      }
      else if ((rollingUpdate >= 20) && (rollingUpdate < 30)) {
        writeObject(USERLED, (rollingUpdate + 8), (!screenData.batterySet[rollingUpdate - 20].returnActive()));
      }
      else {
        writeObject(USERLED, 38, liveOutput);
      }
      Serial.println(rollingUpdate);
      rollingUpdate = rollingUpdate + 1;
      if (rollingUpdate >= 31) {
        rollingUpdate = 0;
      }
      break;
  }
}





