


inline static bool readObject();
inline static bool writeObject(byte object, byte index, unsigned int data);
void updateScreen();
void debugBatteryInfo();
int sumVoltage();

void screenUpdate() {
  if (screenTimer.timerDone()) {
    updateScreen();
    screenTimer.resetTimer();
  }
  else if (verboseTimer.timerDone()) {
    //debugBatteryInfo();

  Serial.print("Battery Voltage: ");
  Serial.println(handheldBatt);
  }
  readObject();
}


void updateScreen() {
  writeToScreen();

}


float sumPWMVoltage() {
  float totalVoltage = 0;
  for (int i = 0; i < 10; i++) {
    switch (pwmSet[i]) {
      case 0:
        continue;
        break;
      case 1:
        totalVoltage += 0.5;
        break;
      case 2:
        totalVoltage += 1;
        break;
      case 3:
        totalVoltage += 2.5;
        break;
      case 4:
        totalVoltage += 3.3;
        break;
    }
  }
  return totalVoltage;
}




int sumVoltage() {
  float total;
  for (int i = 0; i < numCells; i++) {
    total += batterySet[i].returnVoltage();
  }
  return (int) total / 10;
}

inline static bool readStream()
{
  if (Serial3.available() > 5)
  {
    //while bytes are still there
    while (Serial3.available())
    {
      //the the leading byte is not a 0x07 (what we need)
      if (Serial3.peek() != 0x07)
      {
        //eat it out and check again
        Serial3.read();
      }
      else
      {
        //else get outta here (its a 0x07)
        break;
      }
    }
    //if there are at least our 6 byte packet in the stream
    if (Serial3.peek() == 0x07) {
      if (Serial3.available() > 5)
      {
        // get the all 6 of the bytes in the packet
        receivedPack[0] = Serial3.read();
        //calculating the checksum as we go
        checksum  = receivedPack[0];
        receivedPack[1] = Serial3.read();
        checksum ^= receivedPack[1];
        receivedPack[2] = Serial3.read();
        checksum ^= receivedPack[2];
        receivedPack[3] = Serial3.read();
        checksum ^= receivedPack[3];
        receivedPack[4] = Serial3.read();
        checksum ^= receivedPack[4];
        receivedPack[5] = Serial3.read();
        checksum ^= receivedPack[5];
        return true;
      }
    }
  }
  else
    return false;
}

inline static bool writeContrast(int value) {
  Serial3.write(CONTRAST);
  checksum = CONTRAST;
  delayMicroseconds(100);
  Serial3.write((int8_t)value);
  checksum ^= value;
  delayMicroseconds(100);
  Serial3.write(checksum);

  while (!Serial3.available() && !((Serial3.peek() == 0x06) || (Serial3.peek() == 0x15)))
  {
    if (Serial3.available() > 5 && Serial3.peek() == 0x07)
    {
      readObject();
      return false;
    }
  }
  if (Serial3.peek() == 0x06)
  {
    Serial3.read();
    return true;
  }
  else if (Serial3.peek() == 0x15)
  {
    Serial3.read();
    return false;
  }
  else
    return false;
}
//---------------------------------------------------------------------
inline static bool writeObject(byte object, byte index, unsigned int data)
{

  byte lsb = (data >> 0) & 0xFF;
  byte msb = (data >> 8) & 0xFF;
  byte checksum = 0;

  Serial3.write(0x01);
  checksum  = 0x01;
  delayMicroseconds(150);
  Serial3.write(object);
  checksum ^= object;
  delayMicroseconds(150);
  Serial3.write(index);
  checksum ^= index;
  delayMicroseconds(150);
  Serial3.write(msb);
  checksum ^= msb;
  delayMicroseconds(150);
  Serial3.write(lsb);
  checksum ^= lsb;
  delayMicroseconds(150);
  Serial3.write(checksum);
  //  while (!Serial3.available()&& !((Serial3.peek()==0x06) || (Serial3.peek()==0x15)))
  //  {
  //    if(Serial3.available()>5 && Serial3.peek()==0x07)
  //    {
  //      readObject();
  //      return false;
  //    }
  //  }
  //  if (Serial3.peek() == 0x06)
  //  {
  //    Serial3.read();
  //    return true;
  //  }
  //  else if (Serial3.peek() == 0x15)
  //  {
  //    Serial3.read();
  //    return false;
  //  }
  //  else
  //    return false;

}




















