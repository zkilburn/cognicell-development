void updateCommunications() {
  static bool heardBack = false;
  static uint16_t countCommLoss;
  if (rx.receiveData()) {
    digitalWrite(13, !digitalRead(13));



    if (demoState == CHARGINGDC) {
      if (!screenData.completeCharging) {
        if (!screenData.voltageSetOverride) {
          inputData.voltageSet = chargingV;
        } else {
          chargingV = screenData.voltageSetControl;
          inputData.voltageSet = chargingV;
        }
      }
      else {
        liveOutput = false;
        inputData.liveOutput = liveOutput;
        demoState = NONE;
        inputData.commandedState = NONE;
        controlSet = 0;
        inputData.controlSet = 0;
        writeObject(SLIDER, 2, 0);
        motorContactor = false;
        inputData.motorContactor = motorContactor;
        chargingContactor = false;
        inputData.chargingContactor = chargingContactor;
      }
    }
    if (demoState == setPWM) {
      for (int i = 0; i < 10; i++) {
        inputData.pwmSet[i] = pwmSet[i];
      }
    }
    tx.sendData();

    for (int i = 0; i < numCells; i++) {
      batterySet[i] = screenData.batterySet[i];
    }

    dataTimer.resetTimer();
    countCommLoss = 0;
    heardBack = true;
  }

  else if (dataTimer.timerDone()) {
    if (countCommLoss > 2) {
      if (!heardBack) {
        tx.sendData();
      }
      else {
        heardBack = false;
      }
      countCommLoss = 0;
    }
    else countCommLoss++;
    dataTimer.resetTimer();
  }
}
