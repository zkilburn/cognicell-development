#define HandheldBattPin A1

typedef enum demonstrationState {
  NONE = 0, CYCLEDC = 1, POSITIVE = 2, NEGATIVE = 3, CYCLEAC = 4, ALLPOSITIVE = 5, ALLNEGATIVE = 6, ALLBYPASS = 7, ALLFLOAT = 8, SETVOLTAGE = 9, CHARGINGDC = 10, CHARGINGAC = 11, PWMALL = 12, setPWM = 13, CONTROLSET = 14, SOLARCHARGE = 15
};



struct dataIn {
  Cognicell batterySet[12];
  float current;
  uint16_t solarLevel;
  float solarVoltage;
  float cognicellVoltage;
  int8_t voltageSetControl;
  bool voltageSetOverride;
  bool completeCharging;
  uint8_t cellsComplete;
} screenData;

struct dataOut {
  demonstrationState commandedState;
  uint8_t pwmSet[12];
  int8_t voltageSet;
  bool balancedDischarge;
  bool liveOutput;
  uint8_t sub_command;
  uint8_t stopCommand;
  bool chargingContactor;
  bool motorContactor;
  int controlSet;
} inputData;

//SCREEN COMMAND OBJECTS DEFINED
#define FORMMESSAGE 0x0A
#define SLIDER 0x04
#define KNOB 0x01
#define BUTTON 0x06
#define LEDDIGITS 0x0F
#define USERLED 0x13
#define ANGULARMETER 0x07
#define METER 0x10
#define LED 0x0E
#define GUAGE 0x0B
#define STRINGS 0x11
#define SCOPE 0x19
#define KEYBOARD 0x0D
#define CONTRAST 0x04
#define DBUTTON 0x1E

#define recommendedMaxDigits 11
#define lowVoltageDigits 10
#define maxVoltageDigits 9
#define cellCountDigits 8
#define voltageSetpointDigits 7
#define totalPackPotentialDigits 6


#define outputEnableButton 3
#define setpointKeyboard 0
#define outputActiveLED 0
#define angularOutputMeter 0
