#include <digitalWriteFast.h>
#include <EasyTransferCRC.h>
#include <Cognicell.h>
#include <Timers.h>

#define numCells 12

#include "definitions.h"
#include "variables.h"
#include "methods.h"
#include "screen.h"
#include "battery.h"
#include "outgoingScreen.h"
#include "incommingScreen.h"
#include "communications.h"




void setup() {
  //analog battery read
  pinMode(HandheldBattPin,INPUT);
  //LED output
  pinMode(13, OUTPUT);
  //debug
  Serial.begin(230400);
  //screen comms
  Serial3.begin(38400);
  Serial.println("booting");
  //ardu helper comms
  Serial2.begin(115200);
  rx.begin(details(screenData), &Serial2);
  tx.begin(details(inputData), &Serial2);
  writeObject(FORMMESSAGE, 0, 1);
  inputData.balancedDischarge = false;
  delay(1000);
}

void loop() {
  // put your main code here, to run repeatedly:
  updateCommunications();
  screenUpdate();
  updateBattery();
}


void debugBatteryInfo() {
  //Serial.println(" ");
  //Serial.println(" ");
  // Serial.println(" ");
  //  Serial.println(" ");
  //Serial.print(" ");
  Serial.print("Active Form: ");
  Serial.println(activeForm);
  Serial.print("Rolling Update: ");
  Serial.println(rollingUpdate);

  //  Serial.print("Control Set:  ");
  //Serial.println(controlSet);
  //
  //Serial.print("inputdata.controlset:  ");
  //Serial.println(inputData.controlSet);
  //Serial.print("Demo state:  ");
  //Serial.println(demoState);

  //  Serial.println("******Battery Debug*******");
  //  Serial.println(" ");
  //  Serial.println("----Battery 1----");
  //  Serial.print("Voltage    : ");
  //  Serial.println(batterySet[0].returnVoltage());
  //  Serial.print("Temperature:  ");
  //  Serial.println(batterySet[0].returnTemp());
  //  Serial.println(" ");
  //  Serial.println("----Battery 2----");
  //  Serial.print("Voltage    : ");
  //  Serial.println(batterySet[1].returnVoltage());
  //  Serial.print("Temperature:  ");
  //  Serial.println(batterySet[1].returnTemp());
  //  Serial.println(" ");
  //  Serial.println("----Battery 3----");
  //  Serial.print("Voltage    : ");
  //  Serial.println(batterySet[2].returnVoltage());
  //  Serial.print("Temperature:  ");
  //  Serial.println(batterySet[2].returnTemp());
  //  Serial.println(" ");
  //  Serial.println("----Battery 4----");
  //  Serial.print("Voltage    : ");
  //  Serial.println(batterySet[3].returnVoltage());
  //  Serial.print("Temperature:  ");
  //  Serial.println(batterySet[3].returnTemp());
  //  Serial.println("----Battery 5----");
  //  Serial.print("Voltage    : ");
  //  Serial.println(batterySet[4].returnVoltage());
  //  Serial.print("Temperature:  ");
  //  Serial.println(batterySet[4].returnTemp());
  //  Serial.println(" ");
  //  Serial.println("----Cell Stats----");
  //    Serial.print("Lowest Active Cell: ");
  //    Serial.println(activeCellLow+1);
  //    Serial.print("Highest Active Cell: ");
  //    Serial.println(activeCellHigh+1);
  //    Serial.print("Number Active Cells: ");
  //    Serial.println(numCellsActive);

  //  Serial.println("----Battery 6----");
  //  Serial.print("Voltage    : ");
  //  Serial.println(batterySet[5].returnVoltage());
  //  Serial.print("Temperature:  ");
  //  Serial.println(batterySet[5].returnTemp());
  Serial.println(" ");
  Serial.println(" ");
  Serial.println(" ");
  Serial.println(" ");
  Serial.println(" ");
  Serial.println(" ");
}


