
//if packet is present, parse it and instruct robot accordingly
inline static bool readObject()
{
  //Erase the old pack before parsing
  for (int i = 0; i < 6; i++)
  {
    receivedPack[i] = 0;
  }
  readStream();
  //check to see if a packet persists in memory
  // Compare byte 1 to check for CMD Message
  if (receivedPack[0] == 0x07)
  {
    //store the data as a word (not a byte)
    unsigned int dataIn = (receivedPack[3] << 8) + receivedPack[4];
    switch (receivedPack[1])
    {
        //if byte 2 is a button
      case BUTTON:
        switch (receivedPack[2]) {

            //DEMONSTRATION PAGE
            //Cycle +\-
          case 8:
            demoState = CYCLEDC;
            inputData.commandedState = demoState;
            liveOutput = true;
            inputData.liveOutput = liveOutput;
            break;
            //Ramp Positive
          case 9:
            demoState = POSITIVE;
            inputData.commandedState = demoState;
            liveOutput = true;
            inputData.liveOutput = liveOutput;
            break;
            //Ramp Negative
          case 10:
            demoState = NEGATIVE;
            inputData.commandedState = demoState;
            liveOutput = true;
            inputData.liveOutput = liveOutput;
            break;
            //AC simulation
          case 12:
            demoState = CYCLEAC;
            inputData.commandedState = demoState;
            liveOutput = true;
            inputData.liveOutput = liveOutput;
            break;
            //Disable output
          case 17:
            liveOutput = false;
            inputData.liveOutput = liveOutput;
            demoState = NONE;
            inputData.commandedState = demoState;
            inputData.voltageSet = 0;
            vSet = 0;
            // updateBypass();
            break;
            //MAIN PAGE

            //Float all cells
          case 13:
            demoState = ALLFLOAT;
            inputData.commandedState = demoState;
            liveOutput = false;
            inputData.liveOutput = liveOutput;
            chargingContactor = false;
            inputData.chargingContactor = chargingContactor;
            break;
            //Bypass all cells
          case 14:
            demoState = ALLBYPASS;
            inputData.commandedState = demoState;
            liveOutput = false;
            inputData.liveOutput = liveOutput;
            break;
            //Negative all cells
          case 15:
            demoState = ALLNEGATIVE;
            inputData.commandedState = demoState;
            liveOutput = true;
            inputData.liveOutput = liveOutput;
            break;
            //Positive all cells
          case 16:
            demoState = ALLPOSITIVE;
            inputData.commandedState = demoState;
            liveOutput = true;
            inputData.liveOutput = liveOutput;
            break;

            //DC OUTPUT PAGE
            //Enable Disable Output
          case 3:

            demoState = SETVOLTAGE;
            inputData.commandedState = demoState;
            inputData.voltageSet = vSet;
            liveOutput = true;
            inputData.liveOutput = liveOutput;
            break;
          case 21:
            liveOutput = false;
            inputData.liveOutput = liveOutput;
            demoState = NONE;
            inputData.commandedState = demoState;
            inputData.voltageSet = 0;
            vSet = 0;


            break;
            //DC Control 2
            //Turn off output
          case 18:
            liveOutput = false;
            inputData.liveOutput = liveOutput;
            demoState = NONE;
            inputData.commandedState = demoState;
            break;
            //turn output on
          case 19:
            demoState = SETVOLTAGE;
            inputData.commandedState = demoState;
            inputData.voltageSet = vSet;
            liveOutput = true;
            inputData.liveOutput = liveOutput;
            break;

            //Float cells
          case 26:
            chargingV = 0;
            liveOutput = false;
            inputData.liveOutput = liveOutput;
            demoState = ALLFLOAT;
            inputData.commandedState = demoState;
            break;
            //Charging algorithm
          case 24:
            if (chargingV > 0) {
              demoState = CHARGINGDC;
              inputData.voltageSet = chargingV;
            }
            else {
              demoState = SOLARCHARGE;
            }
            inputData.commandedState = demoState;
            liveOutput = true;
            inputData.liveOutput = liveOutput;
            break;

            //More charging voltage
          case 27:
            if ((chargingV) < (numCells * 3)) {
              chargingV += 3;
              if (demoState == CHARGINGDC) {
                inputData.voltageSet = chargingV;
              }
            }
            break;


            //Less chargin voltage
          case 28:
            if ((chargingV) > 0) {
              chargingV -= 3;
            }
            if (demoState == CHARGINGDC) {
              inputData.voltageSet = chargingV;
            }
            break;
            //
          case 30:
            demoState = PWMALL;
            inputData.commandedState = demoState;
            liveOutput = true;
            inputData.liveOutput = liveOutput;
            break;


            //PWM configuration page
            //button minus/plus
            //Minus 1
          case 32:
            if (pwmSet[0] > 0) {
              pwmSet[0]--;
            }
            break;
            //Minus 2
          case 33:
            if (pwmSet[1] > 0) {
              pwmSet[1]--;
            }
            break;
            //Minus 3
          case 34:
            if (pwmSet[2] > 0) {
              pwmSet[2]--;
            }
            break;
            //Minus 4
          case 35:
            if (pwmSet[3] > 0) {
              pwmSet[3]--;
            }
            break;
            //Minus 5
          case 36:
            if (pwmSet[4] > 0) {
              pwmSet[4]--;
            }
            break;
            //Minus 6
          case 37:
            if (pwmSet[5] > 0) {
              pwmSet[5]--;
            }
            break;
            //Minus 7
          case 47:
            if (pwmSet[6] > 0) {
              pwmSet[6]--;
            }
            break;
            //Minus 8
          case 48:
            if (pwmSet[7] > 0) {
              pwmSet[7]--;
            }
            break;
            //Minus 9
          case 49:
            if (pwmSet[8] > 0) {
              pwmSet[8]--;
            }
            break;
            //Minus 10
          case 50:
            if (pwmSet[9] > 0) {
              pwmSet[9]--;
            }
            break;
            //Plus 1
          case 31:
            if (pwmSet[0] < 4) {
              pwmSet[0]++;
            }
            break;
            //Plus 2
          case 38:
            if (pwmSet[1] < 4) {
              pwmSet[1]++;
            }
            break;
            //Plus 3
          case 39:
            if (pwmSet[2] < 4) {
              pwmSet[2]++;
            }
            break;
            //Plus 4
          case 40:
            if (pwmSet[3] < 4) {
              pwmSet[3]++;
            }
            break;
            //Plus 5
          case 41:

            if (pwmSet[4] < 4) {
              pwmSet[4]++;
            }
            break;
            //Plus 6
          case 42:
            if (pwmSet[5] < 4) {
              pwmSet[5]++;
            }
            break;
            //Plus 7
          case 43:
            if (pwmSet[6] < 4) {
              pwmSet[6]++;
            }
            break;
            //Plus 8
          case 44:
            if (pwmSet[7] < 4) {
              pwmSet[7]++;
            }
            break;
            //Plus 9
          case 45:
            if (pwmSet[8] < 4) {
              pwmSet[8]++;
            }
            break;
            //Plus 10
          case 46:
            if (pwmSet[9] < 4) {
              pwmSet[9]++;
            }
            break;
            //Disable output
          case 51:
            for (int i = 0; i < 10; i++) {
              inputData.pwmSet[i] = 0;
              pwmSet[i] = 0;
            }
            liveOutput = false;
            inputData.liveOutput = liveOutput;
            demoState = NONE;
            inputData.commandedState = demoState;
            break;
            //Enable output
          case 52:
            demoState = setPWM;
            inputData.commandedState = demoState;
            for (int i = 0; i < 10; i++) {
              inputData.pwmSet[i] = pwmSet[i];
            }
            liveOutput = true;
            inputData.liveOutput = liveOutput;
            break;
          case 56:
            liveOutput = true;
            inputData.liveOutput = liveOutput;
            chargingContactor = true;
            inputData.chargingContactor = chargingContactor;
            break;
          case 57:
            liveOutput = false;
            inputData.liveOutput = liveOutput;
            chargingContactor = false;
            inputData.chargingContactor = chargingContactor;
            break;
          case 58:
            liveOutput = true;
            inputData.liveOutput = liveOutput;
            motorContactor = true;
            inputData.motorContactor = motorContactor;
            break;
          case 59:
            liveOutput = false;
            inputData.liveOutput = liveOutput;
            motorContactor = false;
            inputData.motorContactor = motorContactor;

            break;
          case 62:
            liveOutput = false;
            inputData.liveOutput = liveOutput;
            demoState = NONE;
            inputData.commandedState = NONE;
            controlSet = 0;
            inputData.controlSet = 0;
            writeObject(SLIDER, 2, 0);
            motorContactor = false;
            inputData.motorContactor = motorContactor;
            chargingContactor = false;
            inputData.chargingContactor = chargingContactor;
            break;
          case 63:
            liveOutput = true;
            demoState = CONTROLSET;
            inputData.liveOutput = liveOutput;
            inputData.controlSet = controlSet;
            inputData.commandedState = demoState;
            vSet = 0;
            break;
          case 68:
            liveOutput = false;
            inputData.liveOutput = liveOutput;
            demoState = NONE;
            inputData.commandedState = NONE;
            controlSet = 0;
            inputData.controlSet = 0;
            writeObject(SLIDER, 2, 0);
            motorContactor = false;
            inputData.motorContactor = motorContactor;
            chargingContactor = false;
            inputData.chargingContactor = chargingContactor;
            break;
          case 65:
            liveOutput = false;
            inputData.liveOutput = liveOutput;
            demoState = NONE;
            inputData.commandedState = NONE;
            controlSet = 0;
            inputData.controlSet = 0;
            writeObject(SLIDER, 2, 0);
            motorContactor = false;
            inputData.motorContactor = motorContactor;
            chargingContactor = false;
            inputData.chargingContactor = chargingContactor;
            break;
          case 71:
            liveOutput = false;
            inputData.liveOutput = liveOutput;
            demoState = NONE;
            inputData.commandedState = NONE;
            controlSet = 0;
            inputData.controlSet = 0;
            writeObject(SLIDER, 2, 0);
            motorContactor = false;
            inputData.motorContactor = motorContactor;
            chargingContactor = false;
            inputData.chargingContactor = chargingContactor;
            break;
          case 72:
            demoState = SETVOLTAGE;
            inputData.commandedState = demoState;
            inputData.voltageSet = vSet;
            liveOutput = true;
            inputData.liveOutput = liveOutput;
            break;
          case 73:
            activeForm = 10;
            writeObject(FORMMESSAGE, 10, 1);
            rollingUpdate = -50;
            break;


        }
        //slider update
      case SLIDER:
        switch (receivedPack[2]) {
            //Positive
          case 0:
            vSet = dataIn * 3;

            break;
            //Negative
          case 1:
            vSet = -(dataIn * 3);
            break;
          case 2:
            controlSet = constrain(((int)(dataIn - 100.0) * ((numCells * 4.0) / 98.0)), -numCells * 4, numCells * 4);
            break;
        }
        if (liveOutput) {
          inputData.controlSet = controlSet;
          inputData.voltageSet = vSet;
        }
        else {

        }
        break;
        //received is a knob
      case KNOB:
        break;
        //else update the form that is currently up
      case FORMMESSAGE:
        activeForm = receivedPack[2];
        rollingUpdate = -15;
        break;
      case KEYBOARD:
        switch (dataIn) {


          case 43:

            if ((vSet) < (numCells * 3)) {
              vSet += 3;
            }
            break;
          case 45:
            if ((vSet) > -(numCells * 3)) {
              vSet -= 3;
            }
            break;
        }
        if (liveOutput) {
          inputData.voltageSet = vSet;
        }
        else {

        }
        break;
      case DBUTTON:
        switch (receivedPack[2]) {
          case 0:
            if (dataIn == 0) {
              inputData.balancedDischarge = false;
            }
            else
              inputData.balancedDischarge = true;
            break;
        }
        break;
    }

  }
}

