uint16_t lookupPOSNEG[7]={470,235,700,670,900,900,555};//{572,381,831,791,1000,1000,570}; 
uint16_t lookupBYPASS[5]={735,775,330,515,540};//{791,831,381,572,608};


//everyMil.h
void everyMil() {
  if (AC.timerDone()) {
    updateCells();
    if (demoState == CYCLEAC) {
      switch (cycle) {
        case 0:
          if(cycle1<5){
          batterySet[cycle1].updateState(POS);
          }
          AC.setInterval(lookupPOSNEG[cycle1]);
          cycle1++;
          if (cycle1 == (5 + 2)) {
            cycle++;
            cycle1 = 0;
          }
          AC.resetTimer();
          break;
        case 1:
          batterySet[cycle1].updateState(BYPASS);
          AC.setInterval(lookupBYPASS[cycle1]);
          cycle1++;
          if (cycle1 == (5)) {
            cycle++;
            cycle1 = 0;
          }
          AC.resetTimer();
          break;
        case 2:
          if(cycle1<5){
          batterySet[cycle1].updateState(NEG);
          }
          AC.setInterval(lookupPOSNEG[cycle1]);
          cycle1++;
          if (cycle1 == (7)) {
            cycle++;
            cycle1 = 0;
          }
          AC.resetTimer();
          break;
        case 3:
          batterySet[cycle1].updateState(BYPASS);
          AC.setInterval(lookupBYPASS[cycle1]);
          cycle1++;
          if (cycle1 == (5)) {
            cycle = 0;
            cycle1 = 0;
          }
          AC.resetTimer();
          break;
        default:
          cycle = 0;
          cycle1 = 0;
          break;
      }
    }

    AC.resetTimer();
  }
}


