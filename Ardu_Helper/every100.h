//every100.h
void everyHundredMil() {
  if (hundredMil.timerDone()) {

    if (demoState == CONTROLSET) {
      if (!controlSetReached) {
        if (powerLevel == controlSet) {
          controlSetReached = true;
        }
        else {

          if (powerLevel > controlSet) {
            adjustDown();
            powerLevel--;
          }
          else {
            adjustUp();
            powerLevel++;
          }
          if (powerLevel == 0) {
            updateBypass();
          }
        }
      }
    }
    else if (demoState == setPWM) {
      bool allGood = false;
      for (int i = 0; i < numCells; i++) {
        if ((inputData.pwmSet[i] != batterySet[i]._pwmSetting) || ((inputData.pwmSet[i] == 0) && (batterySet[i].returnActive()))) {


          if ((inputData.pwmSet[i] != 0)) {
            batterySet[i].updateState(POS, inputData.pwmSet[i]);
                        
          }
          else {
            batterySet[i].updateState(BYPASS);
            batterySet[i]._pwmSetting = 4;            
          }
        }
      }
    }


    hundredMil.resetTimer();
  }

}



