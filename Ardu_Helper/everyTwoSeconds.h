//everyTwoSeconds.h
void everyTwoSeconds() {
  if (twoSeconds.timerDone()) {



    if (demoState == CHARGINGDC) {
      //update which batteries will need to be in what position, based
      //      on which batteries have a higher voltage than others
      updateBatterySelect();
    }
    else if ((demoState == SETVOLTAGE) && inputData.balancedDischarge) {
        //update which batteries will need to be in what position, based
        //      on which batteries have a higher voltage than others
        
        updateBatterySelect();
      }
    else if (demoState == POSITIVE) {
      delayBetweenCells = 1000;
      switch (cycle) {
        case 0:
          updatePositive();
          cycle++;
          break;
        case 1:
          updateBypass();
          cycle = 0;
          break;

        default:
          cycle = 0;
          break;
      }
    }
    else if (demoState == NEGATIVE) {
      delayBetweenCells = 1000;
      switch (cycle) {
        case 0:
          updateNegative();
          cycle++;
          break;
        case 1:
          updateBypass();
          cycle = 0;
          break;

        default:
          cycle = 0;
          break;
      }
    }
    else if (demoState == SOLARCHARGE) {
      selectChargingVoltage();
    }
    twoSeconds.resetTimer();
  }
}


