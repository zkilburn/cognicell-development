//daq.h
bool varIsAbout(int var, int val, int range) {
  return ((var > (val - range)) && (var < (val + range)));
}
void dataGather() {


  //new voltages of cognicell and solar
  float newCurrent = abs((((analogRead(CURRENT) * 0.00488) - 2.5) * 33300) - 440);
  float newSolarVoltage = analogRead(SOLARV) * 0.035;
  float newCogniVoltage = analogRead(COGNIV) * 0.040;
  float newPower = (newCurrent / 1000) * voltsSolar;
  //solar level
  solarLevel = analogRead(SOLARLEVEL);
  screenData.solarLevel = solarLevel;

  //changes in variable increment until algorithm handles them
  dV += newSolarVoltage - voltsSolar;
  dP += newPower - solarPowerOutput;
  dI += (newCurrent - current) / 1000;
  //update variables
  voltsSolar = newSolarVoltage;
  screenData.solarVoltage=voltsSolar;
  solarPowerOutput = newPower;
  voltsCogni = newCogniVoltage;
  screenData.cognicellVoltage=voltsCogni;
  current = newCurrent;
  screenData.current = current;
}


