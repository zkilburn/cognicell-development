/******************************************************************
*  Template for Library newLibrary.h
*	The basic necessities for a functional arduino library		
*
*		Brought to you by:
*              Zac Kilburn
******************************************************************/
#ifndef Cognicell_h
#define Cognicell_h
#include "Arduino.h"
#include "TimersMicros.h"

class Cognicell
{	

public:
	Cognicell(int IO1, int IO2, int OUT, int ENABLE);
	Cognicell();
/******************************************************************
This is where you will declare all of the methods
that will be able to be called by other segments of code externally
******************************************************************/
void updateState(int updatedState);
void updateState(int updatedState,int pwmSetting);
void updatePWM(int pwmSetting);
void swapCells(Cognicell cellOut);
	void setPins(int IO1, int IO2, int OUT, int ENABLE);
	bool readVoltageTemp();
	bool returnActive();
	uint16_t returnVoltage();
	uint16_t returnTemp();
	uint8_t returnPWM();
	int _IO1, _IO2, _OUT, _ENABLE;
uint8_t _pwmSetting;
int _currentState;

bool _cellActive;
private:

	
	
/******************************************************************
Declare all internal variables and methods that will only be called 
and used internally
******************************************************************/

uint16_t _voltage;
uint16_t _temperature;


};
#endif
