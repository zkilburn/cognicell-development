//every250.h
void everyTwoFiftyMil() {
  if (twoFiftyMil.timerDone()) {
    if (demoState != CYCLEAC)
      dataGather();
    if (demoState == SETVOLTAGE) {
      if (vSetMismatchDischarge())
        adjustedLevel = true;
    } else if (demoState == CHARGINGDC) {
      if (vSetMismatchCharge())
        adjustedLevel = true;
    }

    twoFiftyMil.resetTimer();
  }

}



