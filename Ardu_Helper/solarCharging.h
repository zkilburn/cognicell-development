//solarCharging.h"
void selectChargingVoltage() {
  if (!stateChange) {
    if (startSolarCharge) {
      chargeSet = 3;
      startSolarCharge = false;
    }
    else {
      if (dP != 0) {
        if ((dV / dP) > 0) {
          if (chargeSet < (numCells * 3)) {
            updateVoltage(chargeSet + 3);
            chargeSet += 3;
            dV = 0;
            dP = 0;
          }
        }
        else if ((dV / dP) < 0) {
          if (chargeSet > 3) {
            updateVoltage(chargeSet - 3);
            chargeSet -= 3;
            dV = 0;
            dP = 0;
          }
        }
      }
      else if (chargeSet < (numCells * 3)) {
        updateVoltage(chargeSet + 3);
        chargeSet += 3;
        dV = 0;
        dP = 0;
      }

    }
  }
}

