//stateManagement.h
void setupCognicells() {
  // (A,B,BACK,ENABLE)
  //1 
  batterySet[0].setPins(2,3,19,4);
  //2
  batterySet[1].setPins(5,6,19,7);
  //3 -- DOOMM!!
  ///batterySet[2].setPins(2,3,19,10);
  //4
  batterySet[2].setPins(11,12,19,22);
  //5
  batterySet[3].setPins(23,24,19,25);
  //6
  batterySet[4].setPins(26,27,19,28);
  //7
  batterySet[5].setPins(29,30,19,31);
  //8
  batterySet[6].setPins(32,33,19,34);
  //9
  batterySet[7].setPins(35,36,19,37);
  //10
  batterySet[8].setPins(38,39,19,40);
  //11
  batterySet[9].setPins(41,42,19,43);
  //12
  batterySet[10].setPins(44,45,19,46);
  //13
  batterySet[11].setPins(47,48,19,49);
  //14
  batterySet[12].setPins(50,51,19,52);
  
  
//  //conn 11 -- Should support connect
//  batterySet[5].setPins(43, 42, 41, 44);
//  //conn 9
//  batterySet[6].setPins(36, 35, 34, 37);
//  //conn 7
//  batterySet[7].setPins(28, 27, 26, 29);
//  //Conn 1
//  batterySet[8].setPins(4, 3, 2, 5);
//
//
//  //conn 3 -- Should support connect
//  // setPins(12,11,10,13);
// 
//  //conn 5  -- RX TX compromised?
//  //batterySet[7].setPins(20,19,18,21);
//
//
//  //conn 12 -- Should support connect
//  //batterySet[9].setPins(47, 46, 45, 48);
//  //conn 10 ---- wire missing (enable?)
//  batterySet[0].setPins(40, 39, 38, A0);
//  //conn 8
//  batterySet[1].setPins(32, 31, 30, 33);
//  //conn 6
//  batterySet[2].setPins(24,   23, 22, 25); 
//  //conn 4 -- RX TX compromised
//  batterySet[3].setPins(A2,A1,6,A3);
//  //conn 2
//  batterySet[4].setPins(8, 7, 6, 9);
}

void updateCells(){
   for (int i = 0; i < numCells; i++) {
    if(batterySet[i]._pendingUpdate){
      batterySet[i].updatePWM(4);
    } 
      
  }
  
}
//----------------------------------------------------------
//-------------Data collection and manipulation-------------
//----------------------------------------------------------
void updateVoltages() {
  numCellsPresent = 0;
  for (int i = 0; i < numCells; i++) {
    cellPresent[i] = 0;
    if (batterySet[i].readVoltageTemp()) {
      cellPresent[numCellsPresent] = i;
      numCellsPresent++;
    }
  }
}

int sumVoltage() {
  float total;
  for (int i = 0; i < numCells; i++) {
    total += batterySet[i].returnVoltage();
  }
  return (int) total / 10;
}
//----------------------------------------------------------
//-------------Data collection and manipulation--------------
//----------------------------------------------------------



//----------------------------------------------------------
//---------------------Control Algorithms-------------------
//----------------------------------------------------------


void makePositive(int numberCells) {
  for (int i = numberCells; i < numCells; i++) {
    batterySet[i].updateState(BYPASS);
    delay(delayBetweenCells);
  }
  for (int i = 0; i < numberCells; i++) {
    batterySet[i].updateState(POS, 4);
    delay(delayBetweenCells);
  }

}

void makeNegative(int numberCells) {
  for (int i = numberCells; i < numCells; i++) {
    batterySet[i].updateState(BYPASS);
    delay(delayBetweenCells);
  }
  for (int i = 0; i < numberCells; i++) {
    batterySet[i].updateState(NEG, 4);
    delay(delayBetweenCells);
  }
}

void makeFloat(int numberCells) {
  for (int i = numberCells; i < numCells; i++) {
    batterySet[i].updateState(BYPASS);
    delay(delayBetweenCells);
  }
  for (int i = 0; i < numberCells; i++) {
    batterySet[i].updateState(FLOAT);
    delay(delayBetweenCells);
  }
}

//----------------------------------------------------------
//---------------------DC CONTROL ALGORITHMS----------------
//----------------------------------------------------------

void updatePositive() {
  for (int i = numCells-1; i >-1; i--) {
    batterySet[i].updateState(POS, 4);
    delay(delayBetweenCells);
  }
  targetPositive = numCells;
  targetNegative = 0;
  targetBypass = 0;
  targetFloat = 0;
  numCellsActive = numCells;
}

void updateNegative() {
  for (int i = 0; i < numCells; i++) {
    batterySet[i].updateState(NEG, 4);
    delay(delayBetweenCells);
  }
  targetPositive = 0;
  targetNegative = numCells;
  targetBypass = 0;
  targetFloat = 0;
  numCellsActive = numCells;
}

void updateBypass() {
  for (int i = 0; i < numCells; i++) {
    batterySet[i].updateState(BYPASS);
    delay(delayBetweenCells);
  }
  liveOutput = false;
  targetPositive = 0;
  targetNegative = 0;
  targetBypass = numCells;
  targetFloat = 0;
  numCellsActive = 0;
}

void updateFloat() {
  for (int i = 0; i < numCells; i++) {
    batterySet[i].updateState(FLOAT);
    delay(delayBetweenCells);
  }
  liveOutput = false;
  targetPositive = 0;
  targetNegative = 0;
  targetBypass = 0;
  targetFloat = numCells;
  numCellsActive = 0;
}
//----------------------------------------------------------
//------------------------DC CONTROL ALGORITHMS-------------
//----------------------------------------------------------


//----------------------------------------------------------
//-----------------------AC CONTROL ALGORITHMS--------------
//----------------------------------------------------------
void updatePositiveAC() {
  for (int i = 0; i < numCells; i++) {
    batterySet[i].updateState(POS, 4);
    delayMicroseconds(30);
  }
}
void updateNegativeAC() {
  for (int i = 0; i < numCells; i++) {
    batterySet[i].updateState(NEG, 4);
    delayMicroseconds(30);
  }
}
void updateBypassAC() {
  for (int i = 0; i < numCells; i++) {
    batterySet[i].updateState(BYPASS);
    delayMicroseconds(500);
  }
}
//----------------------------------------------------------
//-------------------AC CONTROL ALGORITHMS------------------
//----------------------------------------------------------






//----------------------------------------------------------
void updateVoltage(int voltageOut) {
  switch (voltageOut) {
    case 3:
      makePositive(1);
      targetPositive = 1;
      targetNegative = 0;
      targetBypass = numCells - 1;
      targetFloat = 0;

      numCellsActive = 1;
      break;
    case 6:
      makePositive(2);
      targetPositive = 2;
      targetNegative = 0;
      targetBypass = numCells - 2;
      targetFloat = 0;

      numCellsActive = 2;
      break;
    case 9:
      makePositive(3);
      targetPositive = 3;
      targetNegative = 0;
      targetBypass = numCells - 3;
      targetFloat = 0;

      numCellsActive = 3;
      break;
    case 12:
      makePositive(4);
      targetPositive = 4;
      targetNegative = 0;
      targetBypass = numCells - 4;
      targetFloat = 0;

      numCellsActive = 4;
      break;
    case 15:
      makePositive(5);
      targetPositive = 5;
      targetNegative = 0;
      targetBypass = numCells - 5;
      targetFloat = 0;

      numCellsActive = 5;
      break;
    case 18:
      makePositive(6);
      targetPositive = 6;
      targetNegative = 0;
      targetBypass = numCells - 6;
      targetFloat = 0;

      numCellsActive = 6;
      break;
    case 21:
      makePositive(7);
      targetPositive = 7;
      targetNegative = 0;
      targetBypass = numCells - 7;
      targetFloat = 0;

      numCellsActive = 7;
      break;
    case 24:
      makePositive(8);
      targetPositive = 8;
      targetNegative = 0;
      targetBypass = numCells - 8;
      targetFloat = 0;

      numCellsActive = 8;
      break;
    case 27:
      makePositive(9);
      targetPositive = 9;
      targetNegative = 0;
      targetBypass = numCells - 9;
      targetFloat = 0;
      break;
      case 30:
      
      makePositive(10);
      targetPositive = 10;
      targetNegative = 0;
      targetBypass = numCells - 10;
      targetFloat = 0;
      break;
      case 33:
      
      makePositive(11);
      targetPositive = 11;
      targetNegative = 0;
      targetBypass = numCells - 11;
      targetFloat = 0;
      break;
      case 36:
      
      makePositive(12);
      targetPositive = 12;
      targetNegative = 0;
      targetBypass = numCells - 12;
      targetFloat = 0;
      break;
    case 0:
      updateBypass();
      liveOutput = false;
      targetPositive = 0;
      targetNegative = 0;
      targetBypass = numCells;
      targetFloat = 0;
      numCellsActive = 0;
      break;
    case -3:
      makeNegative(1);
      targetPositive = 0;
      targetNegative = 1;
      targetBypass = numCells - 1;
      targetFloat = 0;
      numCellsActive = 1;
      break;
    case -6:
      makeNegative(2);
      targetPositive = 0;
      targetNegative = 2;
      targetBypass = numCells - 2;
      targetFloat = 0;
      numCellsActive = 2;
      break;
    case -9:
      makeNegative(3);
      targetPositive = 0;
      targetNegative = 3;
      targetBypass = numCells - 3;
      targetFloat = 0;
      numCellsActive = 3;
      break;
    case -12:
      makeNegative(4);
      targetPositive = 0;
      targetNegative = 4;
      targetBypass = numCells - 4;
      targetFloat = 0;
      numCellsActive = 4;
      break;
    case -15:
      makeNegative(5);

      targetPositive = 0;
      targetNegative = 5;
      targetBypass = numCells - 5;
      targetFloat = 0;
      numCellsActive = 5;
      break;
    case -18:
      makeNegative(6);
      targetPositive = 0;
      targetNegative = 6;
      targetBypass = numCells - 6;
      targetFloat = 0;
      numCellsActive = 6;
      break;
    case -21:
      makeNegative(7);
      targetPositive = 0;
      targetNegative = 7;
      targetBypass = numCells - 7;
      targetFloat = 0;
      numCellsActive = 7;
      break;
    case -24:
      makeNegative(8);
      targetPositive = 0;
      targetNegative = 8;
      targetBypass = numCells - 8;
      targetFloat = 0;
      numCellsActive = 8;
      break;
    case -27:
      makeNegative(9);
      targetPositive = 0;
      targetNegative = 9;
      targetBypass = numCells - 9;
      targetFloat = 0;
      break;
    case -30:
      makeNegative(10);
      targetPositive = 0;
      targetNegative = 10;
      targetBypass = numCells - 10;
      targetFloat = 0;
      break;
    case -33:
      makeNegative(11);
      targetPositive = 0;
      targetNegative = 11;
      targetBypass = numCells - 11;
      targetFloat = 0;
      break;
    case -36:
      makeNegative(12);
      targetPositive = 0;
      targetNegative = 12;
      targetBypass = numCells - 12;
      targetFloat = 0;
      break;
  }
}

//----------------------------------------------------------



