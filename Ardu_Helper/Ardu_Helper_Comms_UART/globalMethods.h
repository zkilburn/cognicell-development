//main loops
void everyMil();
void everyTenMil();
void everyHundredMil();
void everyTwoFiftyMil();
void everyFiveHundoMil();
void everySecond();
void everyTwoSeconds();

//communications
void updateCommunications();

//state management - control
void updateVoltage(int voltageOut);
void updatePositive();
void updateNegative();
void updateBypass();
void updateFloat();

//cell sorting
void updateBatterySelect();
void swapCells(uint8_t replaceThis, uint8_t useThis);

//debug
void debugBatteryInfo();



void dataGather();
