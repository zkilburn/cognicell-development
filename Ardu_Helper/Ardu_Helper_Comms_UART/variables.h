//communications
EasyTransferCRC fromScreen, toScreen;

//battery state management
uint16_t targetPositive, targetNegative, targetBypass, targetFloat;
uint16_t negativeBatts, positiveBatts, bypassBatts, floatBatts;
uint16_t activeCellLow, activeCellHigh;
uint8_t inactiveHigh, inactiveLow, chargingCellHigh;
uint8_t battCondition[6];
Cognicell batterySet[11];
uint16_t volts[6];
uint16_t temp[6];

//globals

static demonstrationState demoState;
Timers mil(1), tenMil(10), fiftyMil(50),hundredMil(100), twoFiftyMil(250), fiveHundoMil(500), second(1000), twoSeconds(5000),fiveSeconds(5000);
int cycle=0;
bool liveOutput=false;
uint8_t numCellsActive=0;
uint8_t numCellsPresent=0;
uint8_t cellPresent[10];
int8_t vSet = 0;
bool stateChange=false;
int delayBetweenCells=50;//800 //500000
bool chargingContactor=false;
bool motorContactor=false;
int powerLevel=0;
int controlSet;
bool controlSetReached=true;

float voltsSolar,voltsCogni,current,solarLevel,solarPowerOutput;
 float dV, dP;
unsigned int chargeSet=3;
