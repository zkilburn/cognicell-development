bool varIsAbout(int var, int val,int range){
 return ((var> (val-range))&&(var<(val+range)));
}
void dataGather(){
  //current calculation  
  current = ((((analogRead(CURRENT) *0.00488)-2.5)*33300)-440);
  screenData.current=current;
  
  //new voltages of cognicell and solar
  float newSolarVoltage = analogRead(SOLARV) * 0.035;
  float newCogniVoltage = analogRead(COGNIV) * 0.040;
  float newPower = (current/1000)*voltsSolar;   
  
  //solar level  
  solarLevel= analogRead(SOLARLEVEL);  
  screenData.solarLevel=solarLevel;
    
   //changes in variable increment until algorithm handles them
  dV+=newSolarVoltage-voltsSolar;
  dP+=newCogniVoltage-solarPowerOutput;
  
  //update variables
  voltsSolar=newSolarVoltage;
  solarPowerOutput= newPower;  
  voltsCogni = newCogniVoltage;
}


