uint8_t findLowPWM() {
  uint8_t lowSetting = 4;
  uint8_t lowCell = 0;
  for (int i = 0; i < numCells; i++) {

  Serial.println(i);
  Serial.println(batterySet[i]._pwmSetting);
//    if (batterySet[i]._pwmSetting == 0) {
//      return i;
//    }
//    else 
    if (batterySet[i]._pwmSetting < lowSetting) {
      Serial.println("Set");

      lowSetting = batterySet[i]._pwmSetting;
      lowCell = i;
    
    }
  }
  
  return lowCell;
}

uint8_t findHighPWM() {
  uint8_t highSetting = 0;
  uint8_t highCell = 0;
  for (int i = 0; i < numCells; i++) {

//    if (batterySet[i]._pwmSetting == 4) {
//      return i;
//    }
//    else 
    if (batterySet[i]._pwmSetting > highSetting) {
      Serial.print("New Battery High:       ");
      Serial.println(i);
      highSetting = batterySet[i]._pwmSetting;
      highCell = i;     
    }
  }
  return highCell;
}

void adjustUp() {
  uint8_t batteryToAdjust;
  uint8_t setLevel;
  if (powerLevel > 0) {
    batteryToAdjust = findLowPWM();
    setLevel = batterySet[batteryToAdjust]._pwmSetting + 1;
    batterySet[batteryToAdjust].updateState(POS, setLevel);
  }
  else if (powerLevel < 0) {
    batteryToAdjust = findHighPWM();
    setLevel = batterySet[batteryToAdjust]._pwmSetting - 1;
    if (setLevel != 0) {
      batterySet[batteryToAdjust].updateState(NEG, setLevel);
    }
    else
      batterySet[batteryToAdjust].updateState(BYPASS);
  }
  else {
    batteryToAdjust = findLowPWM();
    setLevel = batterySet[batteryToAdjust]._pwmSetting + 1;
    batterySet[batteryToAdjust].updateState(POS, setLevel);
  }
//  Serial.print("Power Level: ");
//  Serial.println(powerLevel);
//  Serial.println("--------------------------------");
//  Serial.println("        Battery going up        ");
//  Serial.println("--------------------------------");
//  Serial.print("Battery adjusting:       ");
//  Serial.println(batteryToAdjust);
//  Serial.print("Battery Level Target:    ");
//  Serial.println(setLevel);
//  Serial.print("Battery state:           ");
//  switch (batterySet[batteryToAdjust]._currentState) {
//    case 0:
//      Serial.println("POSITIVE");
//      break;
//    case 1:
//      Serial.println("NEGATIVE");
//      break;
//    case 2:
//      Serial.println("BYPASS");
//      break;
//  }
//  Serial.println("--------------------------------");
}


void adjustDown() {
  uint8_t batteryToAdjust;
  uint8_t setLevel;
  if (powerLevel > 0) {
    batteryToAdjust = findHighPWM();
    setLevel = batterySet[batteryToAdjust]._pwmSetting - 1;
    if (setLevel != 0) {
      batterySet[batteryToAdjust].updateState(POS, setLevel);
    }
    else
      batterySet[batteryToAdjust].updateState(BYPASS);
  }
  else if (powerLevel < 0) {
    batteryToAdjust = findLowPWM();
    setLevel = batterySet[batteryToAdjust]._pwmSetting + 1;
    batterySet[batteryToAdjust].updateState(NEG, setLevel);
  }
  else {
    batteryToAdjust = findLowPWM();
    setLevel = batterySet[batteryToAdjust]._pwmSetting + 1;
    batterySet[batteryToAdjust].updateState(NEG, setLevel);
  }
//  Serial.print("Power Level: ");
//  Serial.println(powerLevel);
//  Serial.println("--------------------------------");
//  Serial.println("        Battery going down        ");
//  Serial.println("--------------------------------");
//  Serial.print("Battery adjusting:       ");
//  Serial.println(batteryToAdjust);
//  Serial.print("Battery Level Target:    ");
//  Serial.println(setLevel);
//  Serial.print("Battery state:           ");
//  switch (batterySet[batteryToAdjust]._currentState) {
//    case 0:
//      Serial.println("POSITIVE");
//      break;
//    case 1:
//      Serial.println("NEGATIVE");
//      break;
//    case 2:
//      Serial.println("BYPASS");
//      break;
//  }

}
