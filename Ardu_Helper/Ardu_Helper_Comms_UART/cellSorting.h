void sortCells() {
  if (demoState != CHARGINGDC) {
    uint16_t cellLowVolts = 4000;
    uint16_t cellHighVolts = 2000;
    for (int i = 0; i < numCells; i++) {
      uint16_t cellIn = batterySet[i].returnVoltage(); 
      if(cellIn==0) continue;
      if (batterySet[i].returnActive()) {
        if (cellIn < cellLowVolts) {
          activeCellLow = i;
          cellLowVolts = cellIn;
        }
      }
      else if (cellIn > cellHighVolts) {
        inactiveHigh = i;
        cellHighVolts = cellIn;
      }
    }
  }
  else {
    uint16_t cellHighVoltsCharging = 2000;
    uint16_t cellLowVoltsBypass = 5000;
    for (int i = 0; i < numCells; i++) {
      uint16_t cellIn = batterySet[i].returnVoltage(); 
      if(cellIn==0) continue;
      if (batterySet[i].returnActive()) {
        if (cellIn > cellHighVoltsCharging) {
          chargingCellHigh = i;
          cellHighVoltsCharging = cellIn;
        }
      }
      else if ((cellIn < cellLowVoltsBypass)&&(cellIn!=0)) {
        inactiveLow = i;
        cellLowVoltsBypass = cellIn;
      }
    }
  }
}


void swapCells(uint8_t replaceThis, uint8_t useThis) {
  if (replaceThis != useThis) {
    batterySet[useThis].swapCells(batterySet[replaceThis]); 
  }
}

void updateBatterySelect() {
  if (demoState != CHARGINGDC) {    
    if ((targetBypass != numCells) && (targetBypass != 0)) {      
      sortCells();
      if (liveOutput) {
        for (int i = 0; i < numCells; i++) {
          uint16_t cellIn = batterySet[i].returnVoltage();
          if(cellIn==0) continue;
          //Serial.print("cellIn: ");
          //Serial.print(i);
          //Serial.print("  ");
          //Serial.println(cellIn );
          //
          //
          //Serial.print("inactiveHigh:  ");
          //Serial.print(inactiveHigh);
          //Serial.print("  ");
          //Serial.print(batterySet[inactiveHigh].returnVoltage() );
          //Serial.print("  ");
          //Serial.println(batterySet[inactiveHigh].returnActive());
          //
          //
          //Serial.print("activeCellLow: ");
          //Serial.print(activeCellLow);
          //Serial.print("  ");
          //Serial.print(batterySet[activeCellLow].returnVoltage() );
          //Serial.print("  ");
          //Serial.println(batterySet[activeCellLow].returnActive());
          if ((batterySet[inactiveHigh].returnVoltage() > batterySet[activeCellLow].returnVoltage()) && (!batterySet[inactiveHigh].returnActive())) {
            swapCells(activeCellLow, inactiveHigh);
            int temp=activeCellLow;
            activeCellLow = inactiveHigh;
            inactiveHigh=temp;
            batterySet[inactiveHigh]._cellActive=false;
            batterySet[activeCellLow]._cellActive=true;            
            break;
          }
          else if ((cellIn > batterySet[activeCellLow].returnVoltage()) && (!batterySet[i].returnActive())) {
            swapCells(activeCellLow, i);
            batterySet[activeCellLow]._cellActive=false;
            batterySet[i]._cellActive=true;  
            activeCellLow = i;
            break;
          }
          else {
          }
        }
      }
    }
  }
  else {
    sortCells();
    if (liveOutput) {
      for (int i = 0; i < numCells; i++) {
        
        if ((batterySet[inactiveLow].returnVoltage() < batterySet[chargingCellHigh].returnVoltage()) && ((!batterySet[inactiveLow].returnActive()) &&(batterySet[chargingCellHigh].returnActive()))) {
          swapCells(chargingCellHigh, inactiveLow);
          batterySet[chargingCellHigh]._cellActive=false;
          batterySet[inactiveLow]._cellActive=true;
          int temp = chargingCellHigh;
     
          chargingCellHigh = inactiveLow;
          inactiveLow=temp;
          break;
        }
        uint16_t cellIn = batterySet[i].returnVoltage();
        if(cellIn==0) continue;
        else if ((cellIn < batterySet[chargingCellHigh].returnVoltage()) && ((!batterySet[i].returnActive())&&(batterySet[chargingCellHigh].returnActive()))) {
          swapCells(chargingCellHigh, i);
          chargingCellHigh = i;
          break;
        }
        else {
        }
      }      
    }
  }
  sortCells();
}
