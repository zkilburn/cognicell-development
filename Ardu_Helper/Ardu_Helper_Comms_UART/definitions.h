//Charge pin
#define CHARGE A8
#define MOTOR A9
#define SOLARV A11
#define COGNIV A12
#define SOLARLEVEL A13
#define CURRENT A14

#define POS 0
#define NEG 1
#define BYPASS 2
#define FLOAT 3

typedef enum demonstrationState{ 
  NONE=0, CYCLEDC=1, POSITIVE=2, NEGATIVE=3, CYCLEAC=4, ALLPOSITIVE=5, ALLNEGATIVE=6, ALLBYPASS=7, ALLFLOAT=8,SETVOLTAGE=9, CHARGINGDC=10, CHARGINGAC=11, PWMALL=12,setPWM=13,CONTROLSET=14,SOLARCHARGE=15};

struct dataOut{
  Cognicell batterySet[10];
  float current;
  uint16_t solarLevel;
}
screenData;

struct dataIn{
  demonstrationState commandedState;
  uint8_t pwmSet[10];
  int8_t voltageSet;
  bool balancedDischarge;
  bool liveOutput;
  uint8_t sub_command;
  uint8_t stopCommand;
  bool chargingContactor;
  bool motorContactor;
  int controlSet;
}
inputData;


