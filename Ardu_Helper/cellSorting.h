//cellSorting.h
void sortCells() {
  if (demoState != CHARGINGDC) {
    uint16_t cellLowVolts = 5000;
    uint16_t cellHighVolts = 2000;
    for (int i = 0; i < numCells; i++) {
      uint16_t cellIn = batterySet[i].returnVoltage();
      if (cellIn == 0) continue;
      if (batterySet[i].returnActive()) {
        if (cellIn < cellLowVolts) {
          activeCellLow = i;
          cellLowVolts = cellIn;
        }
      }
      else if (cellIn > cellHighVolts) {
        inactiveHigh = i;
        cellHighVolts = cellIn;
      }
    }
  }
  else {
    uint16_t cellHighVoltsCharging = 2000;
    uint16_t cellLowVoltsBypass = 5000;


    for (int i = 0; i < numCells; i++) {
      uint16_t cellIn = batterySet[i].returnVoltage();

      if (cellIn == 0) continue;
      else if ((cellIn < lowVoltCharge) && (cellChargingComplete[i])) {
        cellChargingComplete[i] = false;
        cellsCompleteCharging--;
      }
      else if (cellIn > highVoltCharge) {
        if (!cellChargingComplete[i]) {
          cellChargingComplete[i] = true;
          cellsCompleteCharging++;
          //          Serial.print("Another completed cell : ");
          //          Serial.println(i);
        }
        if (batterySet[i].returnActive()) {
          //          Serial.println("  ");
          //          Serial.println("  ");
          //          Serial.println("Okay we are going to want to remove this cell ASAP");
          //          Serial.println("  ");
          //          Serial.print("Battery Cell ");
          //          Serial.print(i);
          //          Serial.println(" has gone over the recommended 3.8v to");
          //          Serial.print("a recorded voltage of ");
          //          Serial.println(cellIn);
          //          Serial.println("  ");
          cellHighVoltsCharging = cellIn;
          chargingCellHigh = i;
          //IMPORTANT FUCKING NOTE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! this may be helpful
          //break;
        }
        continue;
      }
      if (batterySet[i].returnActive()) {
        if (cellIn > cellHighVoltsCharging) {
          chargingCellHigh = i;
          cellHighVoltsCharging = cellIn;
        }
      }
      else if ((cellIn < cellLowVoltsBypass) && (cellIn != 0)) {
        inactiveLow = i;
        cellLowVoltsBypass = cellIn;
      }
    }
  }
}


void swapCells(uint8_t replaceThis, uint8_t useThis) {
  //dont swap the same cell for itself
  if (replaceThis != useThis) {
    batterySet[useThis].swapCells(batterySet[replaceThis]);
  }
}

void updateBatterySelect() {
  if (!adjustedLevel) {
    if (demoState != CHARGINGDC) {
      if ((targetBypass != numCells) && (targetBypass != 0)) {
        sortCells();
        if (liveOutput) {
          for (int i = 0; i < numCells; i++) {
            uint16_t cellIn = batterySet[i].returnVoltage();
            if (cellIn == 0) continue;
            //Serial.print("cellIn: ");
            //Serial.print(i);
            //Serial.print("  ");
            //Serial.println(cellIn );
            //
            //
            //Serial.print("inactiveHigh:  ");
            //Serial.print(inactiveHigh);
            //Serial.print("  ");
            //Serial.print(batterySet[inactiveHigh].returnVoltage() );
            //Serial.print("  ");
            //Serial.println(batterySet[inactiveHigh].returnActive());
            //
            //
            //Serial.print("activeCellLow: ");
            //Serial.print(activeCellLow);
            //Serial.print("  ");
            //Serial.print(batterySet[activeCellLow].returnVoltage() );
            //Serial.print("  ");
            //Serial.println(batterySet[activeCellLow].returnActive());


            //if inactive high is bigger than lowest active and really inactive
            if ((batterySet[inactiveHigh].returnVoltage() > batterySet[activeCellLow].returnVoltage()) && (!batterySet[inactiveHigh].returnActive())) {
              swapCells(activeCellLow, inactiveHigh);
              int temp = activeCellLow;
              activeCellLow = inactiveHigh;
              inactiveHigh = temp;
              batterySet[inactiveHigh]._cellActive = false;
              batterySet[activeCellLow]._cellActive = true;
              break;
            }
            else if ((cellIn > batterySet[activeCellLow].returnVoltage()) && (!batterySet[i].returnActive())) {
              swapCells(activeCellLow, i);
              batterySet[activeCellLow]._cellActive = false;
              batterySet[i]._cellActive = true;
              activeCellLow = i;
              break;
            }
            else {

            }
          }
        }
      }
    }
    else {

      sortCells();
      //      Serial.println(" ");
      //      Serial.println(" ");
      //
      //      Serial.println(" ");
      //      Serial.println(" ");
      //      Serial.println(" ");
      //      Serial.println(" ");
      //      Serial.println("---  Voltage Match ->  Balancing Cells  ---");
      //      Serial.println(" ");
      //      Serial.print("The current number of cells completed charging:  ");
      //      Serial.println(cellsCompleteCharging);
      //      Serial.println(" ");
      //if this battery is less than the highest charging -- and -- the battery is not active and the one charging is
      if (((batterySet[inactiveLow].returnVoltage() < batterySet[chargingCellHigh].returnVoltage()) && (!cellChargingComplete[inactiveLow])) && ((!batterySet[inactiveLow].returnActive()) && (batterySet[chargingCellHigh].returnActive()))) {
        //        Serial.println(" ");
        //        Serial.println("--Using the inactiveLow for a switch-- ");
        //        Serial.println(" ");
        //        Serial.print("inactiveLow:  ");
        //        Serial.print(inactiveLow);
        //        Serial.print("  ");
        //        Serial.print(batterySet[inactiveLow].returnVoltage() );
        //        Serial.print("  ");
        //        Serial.print(batterySet[inactiveLow].returnActive());
        //        Serial.print("  ");
        //        Serial.println(batterySet[inactiveLow]._pwmSetting);
        //        Serial.println(" ");
        //        Serial.print("chargingCellHigh: ");
        //        Serial.print(chargingCellHigh);
        //        Serial.print("  ");
        //        Serial.print(batterySet[chargingCellHigh].returnVoltage() );
        //        Serial.print("  ");
        //        Serial.print(batterySet[chargingCellHigh].returnActive());
        //        Serial.print("  ");
        //        Serial.println(batterySet[chargingCellHigh]._pwmSetting);
        //        Serial.println(" ");
        //        Serial.println(" ");
        swapCells(chargingCellHigh, inactiveLow);
        batterySet[chargingCellHigh]._cellActive = false;
        batterySet[inactiveLow]._cellActive = true;
        int temp = chargingCellHigh;

        chargingCellHigh = inactiveLow;
        inactiveLow = temp;
      }
      else {
        for (int i = 0; i < numCells; i++) {
          uint16_t cellIn = batterySet[i].returnVoltage();

          if (cellIn == 0) continue;
          //if this battery is less than the highest charging -- and -- the battery is not active and the one charging is
          else if (((cellIn < batterySet[chargingCellHigh].returnVoltage()) && (!cellChargingComplete[i])) && ((!batterySet[i].returnActive()) && (batterySet[chargingCellHigh].returnActive()))) {
            //            Serial.println("The inactiveLow battery seems unfit? -- found another cell");
            //            Serial.print("cellIn (going in): ");
            //            Serial.print(i);
            //            Serial.print("  ");
            //            Serial.print(cellIn );
            //            Serial.print("  ");
            //            Serial.print(batterySet[chargingCellHigh].returnActive());
            //            Serial.print("  ");
            //            Serial.println(batterySet[chargingCellHigh]._pwmSetting);
            //            Serial.print("chargingCellHigh : ");
            //            Serial.print(chargingCellHigh);
            //            Serial.print("  ");
            //            Serial.print(batterySet[chargingCellHigh].returnVoltage() );
            //            Serial.print("  ");
            //            Serial.print(batterySet[chargingCellHigh].returnActive());
            //            Serial.print("  ");
            //            Serial.println(batterySet[chargingCellHigh]._pwmSetting);
            //            Serial.println(" ");
            //            Serial.println(" ");

            swapCells(chargingCellHigh, i);
            chargingCellHigh = i;
            break;


          }
        }
      }
    }
  }
  else {
    adjustedLevel = false;
  }

}

bool vSetMismatchDischarge() {
  if (  (vSet != inputData.voltageSet)) {
    sortCells();
    if (vSet > 0) {
      if (vSet < inputData.voltageSet) {
        if (!batterySet[inactiveHigh].returnActive()) {
          batterySet[inactiveHigh].updateState(POS, 4);
          vSet += 3;
        }
      }
      else if (vSet > inputData.voltageSet) {
        if (batterySet[activeCellLow].returnActive()) {
          batterySet[activeCellLow].updateState(BYPASS);
          vSet -= 3;
        }
      }
    } else if (vSet < 0) {
      if (vSet > inputData.voltageSet) {
        if (!batterySet[inactiveHigh].returnActive()) {
          batterySet[inactiveHigh].updateState(NEG, 4);
          vSet -= 3;
        }
      }
      else if (vSet < inputData.voltageSet) {
        if (batterySet[activeCellLow].returnActive()) {
          batterySet[activeCellLow].updateState(BYPASS);
          vSet += 3;
        }
      }
    }
    else {
      if (vSet < inputData.voltageSet) {
        if (!batterySet[inactiveHigh].returnActive()) {
          batterySet[inactiveHigh].updateState(POS, 4);
          vSet += 3;
        }
      }
      else if (vSet > inputData.voltageSet) {
        if (batterySet[inactiveHigh].returnActive()) {
          batterySet[inactiveHigh].updateState(NEG, 4);
          vSet -= 3;
        }
      }
    }
    return true;
  } else return false;
}
bool vSetMismatchCharge() {
  uint8_t cellsAvailable = numCells - cellsCompleteCharging;
  if (cellsCompleteCharging != 0) {
    if ( (((cellsAvailable * 3) < ((inputData.voltageSet) * 2)) || (!(cellsAvailable >= (numCells / 2))))     ) {
      if (inputData.voltageSet > 3) {
        screenData.voltageSetOverride = true;
        screenData.voltageSetControl = inputData.voltageSet - 3;
      } else {
        screenData.completeCharging = true;
        updateFloat();
        screenData.voltageSetOverride = false;
        screenData.voltageSetControl = 0;
      }
    }
  }
  if (  (vSet != inputData.voltageSet) ) {
    sortCells();
    if (vSet > 0) {
      if (vSet < inputData.voltageSet) {
        if (!batterySet[inactiveLow].returnActive()) {
          batterySet[inactiveLow].updateState(POS, 4);
          vSet += 3;
        }
      }
      else if (vSet > inputData.voltageSet) {
        if (batterySet[chargingCellHigh].returnActive()) {
          batterySet[chargingCellHigh].updateState(BYPASS);
          vSet -= 3;
        }
      }
    } else if (vSet < 0) {
      if (vSet > inputData.voltageSet) {
        if (!batterySet[inactiveLow].returnActive()) {
          batterySet[inactiveLow].updateState(NEG, 4);
          vSet -= 3;
        }
      }
      else if (vSet < inputData.voltageSet) {
        if (batterySet[chargingCellHigh].returnActive()) {
          batterySet[chargingCellHigh].updateState(BYPASS);
          vSet += 3;
        }
      }
    }
    else {
      if (vSet < inputData.voltageSet) {
        if (!batterySet[inactiveLow].returnActive()) {
          batterySet[inactiveLow].updateState(POS, 4);
          vSet += 3;
        }
      }
      else if (vSet > inputData.voltageSet) {
        if (batterySet[inactiveLow].returnActive()) {
          batterySet[inactiveLow].updateState(NEG, 4);
          vSet -= 3;
        }
      }
    }
    return true;
  }
  else {

    return false;
  }
}
