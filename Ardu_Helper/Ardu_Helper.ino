///Ardu_Helper.ino
uint8_t numCells = 12;
#define highVoltCharge 3600
#define lowVoltCharge 3400

#include <digitalWriteFast.h>
#include <EasyTransferCRC.h>
#include <Cognicell.h>
#include <Timers.h>


#include "definitions.h"
#include "variables.h"
#include "globalMethods.h"
#include "communications.h"
#include "cellSorting.h"
#include "stateManagement.h"
#include "solarCharging.h"
#include "PWM.h"
#include "daq.h"
#include "debug.h"
#include "everyMil.h"
#include "every10.h"
#include "every50.h"
#include "every100.h"
#include "every250.h"
#include "every500.h"
#include "everySecond.h"
#include "everyTwoSeconds.h"


void loop() { 
 
  everyMil();           //ac control
 
  if(demoState!=CYCLEAC){
  everyTenMil();         //main demo state check -- comms check
  //everyFiftyMil();
  everyHundredMil();
  everyTwoFiftyMil();
  //everyFiveHundoMil();
  
  everySecond();         //cell sorting / demo control
  everyTwoSeconds();    //cell sorting / demo control  
  }
}

void setup() {
  
  //Contactor Charging
  pinMode(CHARGE, OUTPUT);
  digitalWriteFast(CHARGE, LOW);
  //Contactor Motor
  pinMode(MOTOR, OUTPUT);
  digitalWriteFast(MOTOR, LOW);
  pinMode(A10, OUTPUT);
  digitalWriteFast(A10, LOW);
 
  pinMode(A11, INPUT);
  pinMode(A12, INPUT);
  pinMode(A13, INPUT);
  pinMode(A14, INPUT);

  Serial.begin(230400);

  Serial.println(" ");
  Serial.println(" ");
  Serial.println(" ");
  Serial.print("Booting System");
  Serial.println(" . ");
  
  //Bluetooth
  Serial2.begin(115200);
  //Cell communications
  Serial1.begin(19200);
  
  Serial.println(" . ");
  pinMode(13, OUTPUT);
  digitalWriteFast(13, LOW);
  Serial.println(" . ");
  toScreen.begin(details(screenData), &Serial2);
  fromScreen.begin(details(inputData), &Serial2);
  Serial.println(" . ");
  pinMode(19, INPUT_PULLUP);
  setupCognicells();
  Serial.println(" . ");
  delay(100);
  updateBypass();
  Serial.println(" . ");
  Serial1.flush();
  updateVoltages();

  Serial.println(" . ");
  inputData.balancedDischarge = false;
  //demoState=CYCLEAC;
}




