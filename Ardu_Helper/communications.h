//communications.h
void updateCommunications() {
  static uint16_t disconnectTime;
  if (fromScreen.receiveData()) {

    digitalWrite(13, !digitalRead(13));

    //stored vs. incomming
    if (demoState != inputData.commandedState) {
      stateChange = true;
      demoState = inputData.commandedState;
    }
    //If the voltage set is changed and the output is live
    if (   (     (     (liveOutput != inputData.liveOutput))    &&     (inputData.liveOutput)    )      &&       (demoState == SETVOLTAGE)  ) {
      updateVoltage(inputData.voltageSet);
      vSet = inputData.voltageSet;
    }

    liveOutput = inputData.liveOutput;
    //vSet=inputData.voltageSet;
    disconnectTime = 0;
    if ((controlSet != inputData.controlSet)) {
      controlSet = inputData.controlSet;
      controlSetReached = false;
    }

    //update contactor control
    if ((inputData.chargingContactor != chargingContactor) && (!motorContactor) && (inputData.chargingContactor != digitalRead(CHARGE))) {
      chargingContactor = inputData.chargingContactor;
      digitalWrite(CHARGE, chargingContactor);
    }
    else if ((inputData.motorContactor != motorContactor) && (!chargingContactor) && (inputData.motorContactor != digitalRead(MOTOR))) {
      motorContactor = inputData.motorContactor;
      digitalWrite(MOTOR, motorContactor);
    }



    //Send stuff
    for (int i = 0; i < numCells; i++) {
      screenData.batterySet[i] = batterySet[i];
    }
    screenData.cellsComplete = cellsCompleteCharging;
    toScreen.sendData();



    
  }
  else
  {
    if (disconnectTime < 200) {
      disconnectTime++;
    }
    else if (disconnectTime == 200) {
      demoState = NONE;
      digitalWriteFast(MOTOR, LOW);
      digitalWriteFast(CHARGE, LOW);
      vSet = 0;
      updateBypass();
      disconnectTime++;
    }
  }

}



