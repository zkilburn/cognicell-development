#include <Cognicell.h>
#include "TimersMicros.h"
#include "Arduino.h"
#include <digitalWriteFast.h>
//Library constructor
Cognicell::Cognicell(int IO1, int IO2, int OUT, int ENABLE)
{
  _cellActive = false;
  _IO1 = IO1;
  _IO2 = IO2;
  _OUT = OUT;
  _ENABLE = ENABLE;
  pinMode(_IO1, OUTPUT);
  pinMode(_IO2, OUTPUT);
  pinMode(_ENABLE, OUTPUT);
  pinMode(_OUT, INPUT_PULLUP);

  digitalWriteFast(_ENABLE, LOW);
  digitalWriteFast(_IO1, LOW);
  digitalWriteFast(_IO2, LOW);
  _currentState = 2;
  _pwmSetting = 0;
  //Serial.begin(9600);
}
Cognicell::Cognicell()
{}

//swap cells
void Cognicell::swapCells(Cognicell cellOut) {
  int _replaceState = cellOut._currentState;
  if (cellOut._IO1 != _IO1) {
    //prep battery one for positive/negative set awaiting PWM signal
    if (_replaceState == 0) {
      digitalWriteFast(_IO1, LOW);
      digitalWriteFast(_IO2, LOW);
      _currentState = 0;
    }
    else if (_replaceState == 1) {
      digitalWriteFast(_IO1, LOW);
      digitalWriteFast(_IO2, HIGH);
      _currentState = 1;
    }
    //Bypass the cell out
    digitalWriteFast(cellOut._IO1, HIGH);
    digitalWriteFast(cellOut._IO2, LOW);
    digitalWriteFast(cellOut._ENABLE, HIGH);
    delayMicroseconds(50);
    digitalWriteFast(_ENABLE, HIGH);
    delayMicroseconds(20);
    digitalWriteFast(_ENABLE, LOW);
    digitalWriteFast(_IO1, LOW);
    digitalWriteFast(_IO2, LOW);
    digitalWriteFast(cellOut._IO1, LOW);
    digitalWriteFast(cellOut._IO2, LOW);
    digitalWriteFast(cellOut._ENABLE, LOW);
    //snap the cell in
    delayMicroseconds(600);
    switch (cellOut._pwmSetting) {
      case 4:
        //00 off
        digitalWriteFast(_IO1, LOW);
        digitalWriteFast(_IO2, LOW);
        digitalWriteFast(_ENABLE, HIGH);
        //delayMicroseconds(1);
        break;
      case 1:
        //01 25

        digitalWriteFast(_IO1, LOW);
        digitalWriteFast(_IO2, HIGH);
        digitalWriteFast(_ENABLE, HIGH);
        //delayMicroseconds(1);
        break;
      case 2:
        //10 50
        digitalWriteFast(_IO1, HIGH);
        digitalWriteFast(_IO2, LOW);
        digitalWriteFast(_ENABLE, HIGH);
        //delayMicroseconds(1);
        break;
      case 3:
        //11 75
        digitalWriteFast(_IO1, HIGH);
        digitalWriteFast(_IO2, HIGH);
        digitalWriteFast(_ENABLE, HIGH);
        //delayMicroseconds(1);
        break;
    }
    _pwmSetting = cellOut._pwmSetting;
    cellOut._pwmSetting = 0;
    cellOut._currentState = 2;
    cellOut._cellActive = false;
    _cellActive = true;
    digitalWriteFast(_IO1, LOW);
    digitalWriteFast(_IO2, LOW);
    digitalWriteFast(_ENABLE, LOW);
  }
}
//
void Cognicell::setPins(int IO1, int IO2, int OUT, int ENABLE)
{
  _cellActive = false;
  _IO1 = IO1;
  _IO2 = IO2;
  _OUT = OUT;
  _ENABLE = ENABLE;
  pinMode(_IO1, OUTPUT);
  pinMode(_IO2, OUTPUT);
  pinMode(_ENABLE, OUTPUT);
  pinMode(_OUT, INPUT_PULLUP);

  digitalWriteFast(_ENABLE, LOW);
  digitalWriteFast(_IO1, LOW);
  digitalWriteFast(_IO2, LOW);
  _currentState = 2;
  _pwmSetting = 0;
  //Serial.begin(9600);
}
void Cognicell::updateState(int updatedState)
{
  switch (updatedState) {

    case 0:
      //00
      digitalWriteFast(_IO1, LOW);
      digitalWriteFast(_IO2, LOW);
      _cellActive = true;
      digitalWriteFast(_ENABLE, HIGH);

      break;
    case 1:
      //01
      digitalWriteFast(_IO1, LOW);
      digitalWriteFast(_IO2, HIGH);
      _cellActive = true;
      digitalWriteFast(_ENABLE, HIGH);
      break;
    case 2:
      //10

      digitalWriteFast(_IO1, HIGH);
      digitalWriteFast(_IO2, LOW);
      _cellActive = false;
      digitalWriteFast(_ENABLE, HIGH);
      _pwmSetting = 0;
      break;
    case 3:
      //11
      digitalWriteFast(_IO1, HIGH);
      digitalWriteFast(_IO2, HIGH);
      _cellActive = false;
      digitalWriteFast(_ENABLE, HIGH);
      _pwmSetting = 0;
      break;
  }
  _currentState = updatedState;
  digitalWriteFast(_ENABLE, LOW);
  digitalWriteFast(_IO1, LOW);
  digitalWriteFast(_IO2, LOW);

  //readVoltage();
}
void Cognicell::updatePWM(int pwmSetting) {
  if ((_currentState != 2) && (_currentState != 3)) {
    //digitalWriteFast(_ENABLE, LOW);
    //delayMicroseconds(5);
    switch (pwmSetting) {

      case 4:
        //00 off
        digitalWriteFast(_IO1, LOW);
        digitalWriteFast(_IO2, LOW);
        digitalWriteFast(_ENABLE, HIGH);
        //delayMicroseconds(1);
        break;
      case 1:
        //01 25
        digitalWriteFast(_IO1, LOW);
        digitalWriteFast(_IO2, HIGH);
        digitalWriteFast(_ENABLE, HIGH);
        //delayMicroseconds(1);
        break;
      case 2:
        //10 50
        digitalWriteFast(_IO1, HIGH);
        digitalWriteFast(_IO2, LOW);
        digitalWriteFast(_ENABLE, HIGH);
        _pwmSetting = 0;
        //delayMicroseconds(1);
        break;
      case 3:
        //11 75
        digitalWriteFast(_IO1, HIGH);
        digitalWriteFast(_IO2, HIGH);
        digitalWriteFast(_ENABLE, HIGH);
        _pwmSetting = 0;
        //delayMicroseconds(1);
        break;
    }
    _pwmSetting = pwmSetting;
    //delayMicroseconds(55);
    digitalWriteFast(_IO1, LOW);
    digitalWriteFast(_IO2, LOW);
    //delayMicroseconds(20);
    digitalWriteFast(_ENABLE, LOW);
  }
}
void Cognicell::updateState(int updatedState, int pwmSetting)
{
  if (pwmSetting == 0) {
    _cellActive = false;
    digitalWriteFast(_IO1, HIGH);
    digitalWriteFast(_IO2, LOW);
    digitalWriteFast(_ENABLE, HIGH);
    _currentState = 2;
    delayMicroseconds(55);
    digitalWriteFast(_IO1, LOW);
    digitalWriteFast(_IO2, LOW);
    delayMicroseconds(20);
    digitalWriteFast(_ENABLE, LOW);

    _pwmSetting = 0;
  }
  else
  {
    switch (updatedState) {
      case 0:
        //00
        _cellActive = true;
        digitalWriteFast(_IO1, LOW);
        digitalWriteFast(_IO2, LOW);
        digitalWriteFast(_ENABLE, HIGH);
        //delayMicroseconds(1);
        break;
      case 1:
        //01
        _cellActive = true;
        digitalWriteFast(_IO1, LOW);
        digitalWriteFast(_IO2, HIGH);
        digitalWriteFast(_ENABLE, HIGH);
        // delayMicroseconds(1);
        break;
      case 2:
        //10
        _cellActive = false;
        digitalWriteFast(_IO1, HIGH);
        digitalWriteFast(_IO2, LOW);
        digitalWriteFast(_ENABLE, HIGH);
        _pwmSetting = 0;
        //delayMicroseconds(1);
        break;
      case 3:
        //11
        _cellActive = false;
        digitalWriteFast(_IO1, HIGH);
        digitalWriteFast(_IO2, HIGH);
        digitalWriteFast(_ENABLE, HIGH);
        _pwmSetting = 0;
        //delayMicroseconds(1);
        break;
    }
    _currentState = updatedState;
    delayMicroseconds(55);
    digitalWriteFast(_IO1, LOW);
    digitalWriteFast(_IO2, LOW);
    delayMicroseconds(20);
    digitalWriteFast(_ENABLE, LOW);

    if ((_currentState != 2) && (_currentState != 3)) {
      delayMicroseconds(500);
      switch (pwmSetting) {
        case 4:
          //00 off
          digitalWriteFast(_IO1, LOW);
          digitalWriteFast(_IO2, LOW);
          digitalWriteFast(_ENABLE, HIGH);
          //delayMicroseconds(1);
          break;
        case 1:
          //01 25

          digitalWriteFast(_IO1, LOW);
          digitalWriteFast(_IO2, HIGH);
          digitalWriteFast(_ENABLE, HIGH);
          //delayMicroseconds(1);
          break;
        case 2:
          //10 50
          digitalWriteFast(_IO1, HIGH);
          digitalWriteFast(_IO2, LOW);
          digitalWriteFast(_ENABLE, HIGH);
          //delayMicroseconds(1);
          break;
        case 3:
          //11 75
          digitalWriteFast(_IO1, HIGH);
          digitalWriteFast(_IO2, HIGH);
          digitalWriteFast(_ENABLE, HIGH);
          //delayMicroseconds(1);
          break;
      }
      _pwmSetting = pwmSetting;
      delayMicroseconds(55);
      digitalWriteFast(_IO1, LOW);
      digitalWriteFast(_IO2, LOW);
      delayMicroseconds(20);
      digitalWriteFast(_ENABLE, LOW);
    }
  }
}

//Used to read the voltage from the cell
bool Cognicell::readVoltageTemp() {
  digitalWriteFast(_ENABLE, LOW);
  while (Serial2.available()) {
    Serial2.read();
  }
  digitalWriteFast(_IO1, HIGH);
  digitalWriteFast(_IO2, HIGH);
  delayMicroseconds(700);
  digitalWriteFast(_IO1, LOW);
  digitalWriteFast(_IO2, LOW);
  delayMicroseconds(400);


  uint8_t count;
  bool heardBack = true;
  while (Serial2.available() < 4) {
    if (count > 15) {
      heardBack = false;
      break;
    }
    count++;
    delay(1);
  }

  if (heardBack) {
    uint16_t first, second, third, fourth;
    //if(Serial2.available()>4){
    first = Serial2.read();
    second = Serial2.read();
    third = Serial2.read();
    fourth = Serial2.read();
    _voltage = 0;
    _voltage = (second & 0x00FF) << 0;
    _voltage = (_voltage | ((first << 8) & 0xFF00));
    _temperature = (fourth & 0x00FF);
    _temperature = (_temperature | ((third << 8) & 0xFF00));
    return true;
  }
  else {
    _temperature = 0;
    _voltage = 0;
    return false;
  }
  return false;
}
uint16_t Cognicell::returnVoltage() {
  return _voltage;
}

uint16_t Cognicell::returnTemp() {
  return _temperature;
}
bool Cognicell::returnActive() {
  return _cellActive;

}
uint8_t Cognicell::returnPWM() {
  return _pwmSetting;
}
