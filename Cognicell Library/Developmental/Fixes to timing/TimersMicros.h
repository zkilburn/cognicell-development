/******************************************************************
*  Timers library for use with Cognicell (DFT)
*	The basic necessities for time keeping	
*     Operates with microSeconds time keeping	
*
*		Brought to you by:
*              Zac Kilburn
******************************************************************/
#ifndef TimersMicros_h
#define TimersMicros_h
#include "Arduino.h"


class TimersMicros
{	
public:
	TimersMicros();
	TimersMicros(int l);
	void resetTimer();
	void setInterval(int l);
	void updateTimer();
	bool timerDone();

private:
	int currentTime;
	int startTime;
	int length;
};
#endif
