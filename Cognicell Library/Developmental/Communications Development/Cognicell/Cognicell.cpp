#include <Cognicell.h>
#include "TimersMicros.h"
#include "Arduino.h"
#include <SoftwareSerial.h>
//Library constructor
Cognicell::Cognicell(int IO1, int IO2, int OUT, int ENABLE)
{
  this->serial=new SoftwareSerial(IO1,13);
  //serial.begin(9600);
  _cellActive = false;
  _IO1 = IO1;
  _IO2 = IO2;
  _OUT = OUT;
  _ENABLE = ENABLE;
  pinMode(_IO1, OUTPUT);
  pinMode(_IO2, OUTPUT);
  pinMode(_ENABLE, OUTPUT);
  pinMode(_OUT, INPUT_PULLUP);

  digitalWrite(_ENABLE, LOW);
  digitalWrite(_IO1, LOW);
  digitalWrite(_IO2, LOW);
  _currentState=2;
  _pwmSetting=0;
  //Serial.begin(9600);
}
Cognicell::Cognicell(int IO1, int IO2, int OUT, int ENABLE,SoftwareSerial *s)
{
  serial=s;
  //serial.begin(9600);
  _cellActive = false;
  _IO1 = IO1;
  _IO2 = IO2;
  _OUT = OUT;
  _ENABLE = ENABLE;
  pinMode(_IO1, OUTPUT);
  pinMode(_IO2, OUTPUT);
  pinMode(_ENABLE, OUTPUT);
  pinMode(_OUT, INPUT_PULLUP);

  digitalWrite(_ENABLE, LOW);
  digitalWrite(_IO1, LOW);
  digitalWrite(_IO2, LOW);
  _currentState=2;
  _pwmSetting=0;
  //Serial.begin(9600);
}
Cognicell::Cognicell()
{}

//swap cells
void Cognicell::swapCells(Cognicell cellOut){
	//serial.write(0x00);
	int _replaceState=cellOut._currentState;
	if (cellOut._IO1 != _IO1) {   	
    	//prep battery one for positive/negative set awaiting PWM signal         
	  if (_replaceState==0) {  
	  digitalWrite(_IO1, LOW);
      digitalWrite(_IO2, LOW);
      _currentState=0;
  }
      else if (_replaceState==1) {
      digitalWrite(_IO1, LOW);
      digitalWrite(_IO2, HIGH);
      _currentState=1;
    }
      
      //Bypass the cell out
      digitalWrite(cellOut._IO1, HIGH);
      digitalWrite(cellOut._IO2, LOW);
      
      digitalWrite(_ENABLE, HIGH);
      delayMicroseconds(1);
      digitalWrite(cellOut._ENABLE, HIGH);
      delayMicroseconds(20);
	  	  digitalWrite(_IO1, LOW);
     digitalWrite(_IO2, LOW);
 	 digitalWrite(_ENABLE, LOW);
  	 digitalWrite(cellOut._IO1, LOW);
     digitalWrite(cellOut._IO2, LOW);
	 digitalWrite(cellOut._ENABLE, LOW);
  	
  	//snap the cell in        
     delayMicroseconds(600);
      switch (cellOut._pwmSetting) {

      case 4:
        //00 off						            	          	  
		digitalWrite(_IO1, LOW);
        digitalWrite(_IO2, LOW);
        digitalWrite(_ENABLE, HIGH);
        //delayMicroseconds(1);   
        break;
      case 1:
        //01 25						
	   
        digitalWrite(_IO1, LOW);
        digitalWrite(_IO2, HIGH);
        digitalWrite(_ENABLE, HIGH);
        //delayMicroseconds(1);   	

        break;
      case 2:
        //10 50       
        digitalWrite(_IO1, HIGH);
        digitalWrite(_IO2, LOW);
        digitalWrite(_ENABLE, HIGH);
        //delayMicroseconds(1);

        break;
      case 3:
        //11 75
        digitalWrite(_IO1, HIGH);
        digitalWrite(_IO2, HIGH);
        digitalWrite(_ENABLE, HIGH);
        //delayMicroseconds(1);

        break;
    }
    
    _pwmSetting=cellOut._pwmSetting;
    cellOut._pwmSetting = 0;   
    
    cellOut._currentState = 2;    
      
      
      
     cellOut._cellActive = false;
     _cellActive = true;    
     
  	 
  	  
  	  
    digitalWrite(_IO1, LOW);
    digitalWrite(_IO2, LOW);      
    digitalWrite(_ENABLE, LOW);
      
    }
    
  }
	

//
void Cognicell::setPins(int IO1, int IO2, int OUT, int ENABLE)
{

  _cellActive = false;
  _IO1 = IO1;
  _IO2 = IO2;
  _OUT = OUT;
  _ENABLE = ENABLE;
  pinMode(_IO1, OUTPUT);
  pinMode(_IO2, OUTPUT);
  pinMode(_ENABLE, OUTPUT);
  pinMode(_OUT, INPUT_PULLUP);

  digitalWrite(_ENABLE, LOW);
  digitalWrite(_IO1, LOW);
  digitalWrite(_IO2, LOW);
   _currentState=2;
  _pwmSetting=0;
  //Serial.begin(9600);
}
//
void Cognicell::setPins(int IO1, int IO2, int OUT, int ENABLE,SoftwareSerial *s)
{
 serial=s;
 serial->begin(9600);
  _cellActive = false;
  _IO1 = IO1;
  _IO2 = IO2;
  _OUT = OUT;
  _ENABLE = ENABLE;
  pinMode(_IO1, OUTPUT);
  pinMode(_IO2, OUTPUT);
  pinMode(_ENABLE, OUTPUT);
  pinMode(_OUT, INPUT_PULLUP);

  digitalWrite(_ENABLE, LOW);
  digitalWrite(_IO1, LOW);
  digitalWrite(_IO2, LOW);
   _currentState=2;
  _pwmSetting=0;
  //Serial.begin(9600);
}
void Cognicell::updateState(int updatedState)
{	
serial->write((uint8_t)0x00);
  switch (updatedState) {

    case 0:
      //00
      digitalWrite(_IO1, LOW);
      digitalWrite(_IO2, LOW);
      _cellActive = true;
      digitalWrite(_ENABLE, HIGH);

      break;
    case 1:
      //01
      digitalWrite(_IO1, LOW);
      digitalWrite(_IO2, HIGH);
      _cellActive = true;
      digitalWrite(_ENABLE, HIGH);
      break;
    case 2:
      //10

      digitalWrite(_IO1, HIGH);
      digitalWrite(_IO2, LOW);
      _cellActive = false;
      digitalWrite(_ENABLE, HIGH);
     _pwmSetting = 0;  
      break;
    case 3:
      //11
      digitalWrite(_IO1, HIGH);
      digitalWrite(_IO2, HIGH);
      _cellActive = false;
      digitalWrite(_ENABLE, HIGH);
      _pwmSetting = 0;  
      break;
  }


  _currentState = updatedState;

  digitalWrite(_ENABLE, LOW);
  digitalWrite(_IO1, LOW);
  digitalWrite(_IO2, LOW);

 

  //readVoltage();
}
void Cognicell::updatePWM(int pwmSetting){
	if ((_currentState != 2) && (_currentState != 3)) {

    //digitalWrite(_ENABLE, LOW);
    //delayMicroseconds(5);
    switch (pwmSetting) {

      case 4:
        //00 off						            	          	   
		digitalWrite(_IO1, LOW);
        digitalWrite(_IO2, LOW);
        digitalWrite(_ENABLE, HIGH);
        //delayMicroseconds(1);   
        break;
      case 1:
        //01 25						
	   
        digitalWrite(_IO1, LOW);
        digitalWrite(_IO2, HIGH);
        digitalWrite(_ENABLE, HIGH);
        //delayMicroseconds(1);   	

        break;
      case 2:
        //10 50       
        digitalWrite(_IO1, HIGH);
        digitalWrite(_IO2, LOW);
        digitalWrite(_ENABLE, HIGH);
     _pwmSetting = 0;  
        //delayMicroseconds(1);

        break;
      case 3:
        //11 75
        digitalWrite(_IO1, HIGH);
        digitalWrite(_IO2, HIGH);
        digitalWrite(_ENABLE, HIGH);
     _pwmSetting = 0;  
        //delayMicroseconds(1);

        break;
    }
    _pwmSetting = pwmSetting;
    //delayMicroseconds(55);
    digitalWrite(_IO1, LOW);
    digitalWrite(_IO2, LOW);
    //delayMicroseconds(20);
    digitalWrite(_ENABLE, LOW);
  }
}
void Cognicell::updateState(int updatedState, int pwmSetting)
{
     if(pwmSetting==0){
                       _cellActive = false;     
 	digitalWrite(_IO1, HIGH);
      digitalWrite(_IO2, LOW);
      digitalWrite(_ENABLE, HIGH);
                       _currentState = 2;
  delayMicroseconds(55);
  digitalWrite(_IO1, LOW);
  digitalWrite(_IO2, LOW);
  delayMicroseconds(20);
  digitalWrite(_ENABLE, LOW);
  
     _pwmSetting = 0;  
                       }
                       else
                       {
  switch (updatedState) {

    case 0:
      //00
      _cellActive = true;      

	  digitalWrite(_IO1, LOW);
      digitalWrite(_IO2, LOW);
      digitalWrite(_ENABLE, HIGH);
      //delayMicroseconds(1);
      break;
    case 1:
      //01
      _cellActive = true;			    	
	digitalWrite(_IO1, LOW);
      digitalWrite(_IO2, HIGH);
      digitalWrite(_ENABLE, HIGH);
     // delayMicroseconds(1);      	
      break;
    case 2:
      //10
      _cellActive = false;     
 	digitalWrite(_IO1, HIGH);
      digitalWrite(_IO2, LOW);
      digitalWrite(_ENABLE, HIGH);
     _pwmSetting = 0;  
      //delayMicroseconds(1);


      break;
    case 3:
      //11
      _cellActive = false;
      digitalWrite(_IO1, HIGH);
      digitalWrite(_IO2, HIGH);
      digitalWrite(_ENABLE, HIGH);
     _pwmSetting = 0;  
      //delayMicroseconds(1);

      break;
  }

  _currentState = updatedState;
  delayMicroseconds(55);
  digitalWrite(_IO1, LOW);
  digitalWrite(_IO2, LOW);
  delayMicroseconds(20);
  digitalWrite(_ENABLE, LOW);

  
  if ((_currentState != 2) && (_currentState != 3)) {
     delayMicroseconds(500);
    switch (pwmSetting) {
      case 4:
        //00 off						            	          	   
		digitalWrite(_IO1, LOW);
        digitalWrite(_IO2, LOW);
        digitalWrite(_ENABLE, HIGH);
        //delayMicroseconds(1);   
        break;
      case 1:
        //01 25						
	   
        digitalWrite(_IO1, LOW);
        digitalWrite(_IO2, HIGH);
        digitalWrite(_ENABLE, HIGH);
        //delayMicroseconds(1);   	

        break;
      case 2:
        //10 50       
        digitalWrite(_IO1, HIGH);
        digitalWrite(_IO2, LOW);
        digitalWrite(_ENABLE, HIGH);
        //delayMicroseconds(1);

        break;
      case 3:
        //11 75
        digitalWrite(_IO1, HIGH);
        digitalWrite(_IO2, HIGH);
        digitalWrite(_ENABLE, HIGH);
        //delayMicroseconds(1);

        break;
    }
    _pwmSetting = pwmSetting;
    delayMicroseconds(55);
    digitalWrite(_IO1, LOW);
    digitalWrite(_IO2, LOW);
    delayMicroseconds(20);
    digitalWrite(_ENABLE, LOW);
  }
  
}

}

//Used to read the voltage from the cell
bool Cognicell::readVoltageTemp() {

  digitalWrite(_ENABLE, LOW);
  while (Serial2.available()) {
    Serial2.read();
  }

  digitalWrite(_IO1, HIGH);
  digitalWrite(_IO2, HIGH);
  delayMicroseconds(700);
  digitalWrite(_IO1, LOW);
  digitalWrite(_IO2, LOW);
  delayMicroseconds(400);


  uint8_t count;
  bool heardBack = true;
  while (Serial2.available() < 4) {

    if (count > 15) {
      heardBack = false;
      break;
    }
    count++;
    delay(1);

  }

  if (heardBack) {
    uint16_t first, second, third, fourth;
    //if(Serial2.available()>4){
    first = Serial2.read();
    second = Serial2.read();
    third = Serial2.read();
    fourth = Serial2.read();
    _voltage = 0;
    _voltage = (second & 0x00FF) << 0;
    _voltage = (_voltage | ((first << 8) & 0xFF00));
    _temperature = (fourth & 0x00FF);
    _temperature = (_temperature | ((third << 8) & 0xFF00));
    return true;
  }
  else {
  	_voltage = 0;
  	_temperature = 0;
     return false;
  }

}
uint16_t Cognicell::returnVoltage() {
  return _voltage;
}

uint16_t Cognicell::returnTemp() {
  return _temperature;
}
bool Cognicell::returnActive() {
  return _cellActive;

}
uint8_t Cognicell::returnPWM() {
  return _pwmSetting;
}
