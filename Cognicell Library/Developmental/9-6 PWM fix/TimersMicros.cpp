#include "TimersMicros.h""

//Library constructor
TimersMicros::TimersMicros()
{	
  length=50;
}
TimersMicros::TimersMicros(int l)
{	
  length=l;
}
//External method
void TimersMicros::resetTimer()
{
	startTime=micros();
}
void TimersMicros::setInterval(int l)
{
	length=l;	
}
void TimersMicros::updateTimer()
{
	currentTime=micros();
}
bool TimersMicros::timerDone()
{
	currentTime=micros();
	if ((currentTime-startTime)>length)
	{
		startTime=micros();
		return true;
	}
	else return false;
}
